package megaapps.com.br.ujungo.FragmentView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import megaapps.com.br.ujungo.ActivityView.HomeActivity;
import megaapps.com.br.ujungo.Extendables.MyFragment;
import megaapps.com.br.ujungo.Presenter.AddAddressPresenter;
import megaapps.com.br.ujungo.R;
import megaapps.com.br.ujungo.Util.CustomFontButton;
import megaapps.com.br.ujungo.Util.CustomFontEditText;
import megaapps.com.br.ujungo.Util.CustomFontTextView;

/**
 * Created by duh on 6/28/17.
 */

public class AddAddressFragment extends MyFragment {

    @BindView(R.id.layLoading)
    RelativeLayout layLoading;
    @BindView(R.id.scrowData)
    ScrollView scrowData;
    @BindView(R.id.tittleLoading)
    CustomFontTextView tittleLoading;
    @BindView(R.id.editAddress)
    CustomFontEditText editAddress;
    @BindView(R.id.input_layout_address)
    TextInputLayout inputLayoutAddress;
    @BindView(R.id.editNumber)
    CustomFontEditText editNumber;
    @BindView(R.id.input_layout_number)
    TextInputLayout inputLayoutNumber;
    @BindView(R.id.editNeighborhood)
    CustomFontEditText editNeighborhood;
    @BindView(R.id.input_layout_neighborhood)
    TextInputLayout inputLayoutNeighborhood;
    @BindView(R.id.editCep)
    CustomFontEditText editCep;
    @BindView(R.id.input_layout_cep)
    TextInputLayout inputLayoutCep;
    @BindView(R.id.editCity)
    CustomFontEditText editCity;
    @BindView(R.id.input_layout_city)
    TextInputLayout inputLayoutCity;
    @BindView(R.id.editState)
    CustomFontEditText editState;
    @BindView(R.id.input_layout_state)
    TextInputLayout inputLayoutState;
    @BindView(R.id.editCountry)
    CustomFontEditText editCountry;
    @BindView(R.id.input_layout_country)
    TextInputLayout inputLayoutCountry;
    @BindView(R.id.btnSave)
    CustomFontButton btnSave;
    @BindView(R.id.openDrawer)
    Button openDrawer;
    Unbinder unbinder;

    private String id;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private AddAddressPresenter addAddressPresenter;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_address, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((HomeActivity) getActivity()).closeDrawer();

        pref = getContext().getSharedPreferences("login.conf", Context.MODE_PRIVATE);
        id = pref.getString("uid", "");
        addAddressPresenter = new AddAddressPresenter(this);
        addAddressPresenter.load(id);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnSave, R.id.openDrawer})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.btnSave:
                addAddressPresenter.getDataUpdateAddress(
                        id,
                        editAddress.getText().toString(),
                        editNumber.getText().toString(),
                        editNeighborhood.getText().toString(),
                        editCep.getText().toString(),
                        editCity.getText().toString(),
                        editState.getText().toString(),
                        editCountry.getText().toString());
                break;

            case R.id.openDrawer:
                ((HomeActivity) getActivity()).openDrawer();
                break;

        }
    }

    public void emptyAddress() {

        inputLayoutAddress.requestFocus();
        editAddress.setError(getText(R.string.emptyAddress));
    }

    public void emptyNumber() {
        inputLayoutNumber.requestFocus();
        editNumber.setError(getText(R.string.emptyNumber));
    }

    public void emptyNeighborhood() {
        inputLayoutNeighborhood.requestFocus();
        editNeighborhood.setError(getText(R.string.emptyNeighborhood));
    }

    public void emptyCep() {
        inputLayoutCep.requestFocus();
        editCep.setError(getText(R.string.emptyCep));
    }

    public void emptyCity() {
        inputLayoutCity.requestFocus();
        editCity.setError(getText(R.string.emptyCity));
    }

    public void emptyState() {
        inputLayoutState.requestFocus();
        editState.setError(getText(R.string.emptyState));
    }

    public void emptyCountry() {
        inputLayoutCountry.requestFocus();
        editCountry.setError(getText(R.string.emptyCountry));
    }

    public void updateOk() {
        Toast.makeText(getContext(), R.string.requestsuccess, Toast.LENGTH_LONG).show();
    }

    public void loadOK(String address, String number, String neighborhood, String zipcode, String city, String state, String country) {

        editAddress.setText(address);
        editNumber.setText(number);
        editNeighborhood.setText(neighborhood);
        editCep.setText(zipcode);
        editCity.setText(city);
        editState.setText(state);
        editCountry.setText(country);

    }

    public void execLoading() {
        layLoading.setVisibility(View.VISIBLE);
        scrowData.setVisibility(View.GONE);
    }

    public void stopLoading() {

        layLoading.setVisibility(View.GONE);
        scrowData.setVisibility(View.VISIBLE);

    }

}
