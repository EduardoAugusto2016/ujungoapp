package megaapps.com.br.ujungo.Presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import megaapps.com.br.ujungo.Model.RequestDeliveryModel;
import megaapps.com.br.ujungo.Service.APIService;
import megaapps.com.br.ujungo.FragmentView.PublicationsDeliveryFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by duh on 7/15/17.
 */

public class PublicationsDeliveryPresenter {

    //private static String BASE_URL = "http://10.0.3.2/";
    private static String BASE_URL = "http://easytvteste2017.esy.es/";
    private List<RequestDeliveryModel> requestDeliveryModel;
    private PublicationsDeliveryFragment publicationsDeliveryFragment;
    private List<RequestDeliveryModel> newRequestDeliveryModel  = new ArrayList<>();


    public PublicationsDeliveryPresenter(PublicationsDeliveryFragment publicationsDeliveryFragment) {
        this.publicationsDeliveryFragment = publicationsDeliveryFragment;
    }

    public void getDataDelManRequest() {

        publicationsDeliveryFragment.execLoading();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<List<RequestDeliveryModel>> call = service.loadDelManRequest();
        call.enqueue(new Callback<List<RequestDeliveryModel>>() {
            @Override
            public void onResponse(Call<List<RequestDeliveryModel>> call, Response<List<RequestDeliveryModel>> response) {

                if (response.isSuccessful()) {

                    requestDeliveryModel = response.body();

                    if (requestDeliveryModel != null) {

                        for (int i = 0; i < requestDeliveryModel.size(); i++) {

                            if(requestDeliveryModel.get(i).getAccept().equals("false")){

                                newRequestDeliveryModel.add(requestDeliveryModel.get(i));

                            }

                            publicationsDeliveryFragment.loadDelManRequest(newRequestDeliveryModel);

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<RequestDeliveryModel>> call, Throwable t) {
                publicationsDeliveryFragment.stopLoading();
            }
        });
    }
}
