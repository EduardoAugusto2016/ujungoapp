package megaapps.com.br.ujungo.Presenter;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import megaapps.com.br.ujungo.Model.UserModel;
import megaapps.com.br.ujungo.Service.APIService;
import megaapps.com.br.ujungo.ActivityView.LoginActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by duh on 6/21/17.
 */

public class LoginPresenter {

    //private static String BASE_URL = "http://10.0.3.2/";
    private static String BASE_URL = "http://easytvteste2017.esy.es/";
    private LoginActivity loginActivity;
    private UserModel userModel;

    public LoginPresenter(LoginActivity loginActivity) {
        this.loginActivity = loginActivity;
    }

    public void getDataLogin(String email, String password) {

        if(email.isEmpty() || password.isEmpty()){

            if(email.isEmpty()){
                loginActivity.emailEmpty();
                return;
            }

            if(password.isEmpty()){
                loginActivity.passwordEmpty();
                return;
            }

        }else{

            loginActivity.execLoading();
            login(email, password);

        }
    }

    private void login(String email, String password) {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<UserModel> call = service.login(email, password);
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {

                if (response.isSuccessful()) {

                    userModel = response.body();

                    if (userModel.getResultCode().equals("OK")) {

                        String id = userModel.getId();
                        String name = userModel.getName();
                        String avatar = userModel.getAvatar();

                        loginActivity.loginOk(id, name, avatar);

                    } else if (userModel.getResultCode().equals("NO")) {

                        loginActivity.stopLoading();
                        loginActivity.loginError();

                    }
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {

                Log.i("error_login", t.getMessage());
                loginActivity.stopLoading();

            }
        });

    }
}
