package megaapps.com.br.ujungo.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import megaapps.com.br.ujungo.Model.RequestDeliveryModel;
import megaapps.com.br.ujungo.R;
import megaapps.com.br.ujungo.ActivityView.HomeActivity;
import megaapps.com.br.ujungo.FragmentView.ItemAcceptAddFragment;

/**
 * Created by duh on 7/15/17.
 */

public class PublicationDeliveryAdapter extends RecyclerView.Adapter<PublicationDeliveryAdapter.ReqDelManPubRecyclerHolder> {

    Context context;
    private List<RequestDeliveryModel> requestDeliveryList;
    private HomeActivity homeactivity;

    private String id;

    public PublicationDeliveryAdapter(Context context, List<RequestDeliveryModel> requestDeliveryModel) {
        this.context = context;
        this.requestDeliveryList = requestDeliveryModel;
    }

    @Override
    public ReqDelManPubRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_publications_deliveryman, parent, false);
        return new ReqDelManPubRecyclerHolder(view);
    }

    @Override
    public void onBindViewHolder(final ReqDelManPubRecyclerHolder holder, final int position) {

        ReqDelManPubRecyclerHolder viewHolder = (ReqDelManPubRecyclerHolder) holder;

        viewHolder.txtTittle.setText(requestDeliveryList.get(position).getTittle());
        viewHolder.txtAddress.setText("Destino: " +
                requestDeliveryList.get(position).getCityPub() + " - " +
                requestDeliveryList.get(position).getStatePub()
        );
        viewHolder.txtDate.setText("Entregar até: "+requestDeliveryList.get(position).getDatePub());
        viewHolder.txtValue.setText("R$ "+requestDeliveryList.get(position).getValuePub());

        Glide.with(context).load(requestDeliveryList.get(position).getImage()).into(viewHolder.imgAnnounce);

        if(requestDeliveryList.get(position).getSize().equals("pequeno")){
            Glide.with(context).load(R.drawable.ic_item_one_small_gray).into(viewHolder.imgSize);
        }
        if(requestDeliveryList.get(position).getSize().equals("medio")){
            Glide.with(context).load(R.drawable.ic_item_two_small_gray).into(viewHolder.imgSize);
        }
        if(requestDeliveryList.get(position).getSize().equals("grande")){
            Glide.with(context).load(R.drawable.ic_item_tree_small_gray).into(viewHolder.imgSize);
        }
        if(requestDeliveryList.get(position).getSize().equals("enorme")){
            Glide.with(context).load(R.drawable.ic_item_four_small_gray).into(viewHolder.imgSize);
        }
        // esse trem aqui clicando nele vai abrir a outra tela (fragment)
        ((ReqDelManPubRecyclerHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // na outra tela tem que ter um getter e setter de id ai tem que passar esse ai ai  embaixo.
                requestDeliveryList.get(position).getId();
                homeactivity = (HomeActivity) context;
                ItemAcceptAddFragment frag = new ItemAcceptAddFragment();
                FragmentTransaction ft = homeactivity.getSupportFragmentManager().beginTransaction();
                ft.addToBackStack("acceptAdd");
                ft.replace(R.id.frContent,frag);
                ft.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return requestDeliveryList.size();
    }

    public class ReqDelManPubRecyclerHolder extends RecyclerView.ViewHolder {

        public TextView txtTittle, txtDate, txtAddress, txtValue;
        public ImageView imgAnnounce, imgSize;

        ReqDelManPubRecyclerHolder(View itemView) {
            super(itemView);

            imgAnnounce = (ImageView) itemView.findViewById(R.id.imgAnnounce);
            txtTittle = (TextView) itemView.findViewById(R.id.txtTittle);
            txtAddress = (TextView) itemView.findViewById(R.id.txtAddress);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtValue = (TextView) itemView.findViewById(R.id.txtValue);
            imgSize = (ImageView) itemView.findViewById(R.id.imgSize);

        }
    }
}
