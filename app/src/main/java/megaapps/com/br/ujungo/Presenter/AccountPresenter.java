package megaapps.com.br.ujungo.Presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import megaapps.com.br.ujungo.FragmentView.AccountFragment;
import megaapps.com.br.ujungo.Model.UserModel;
import megaapps.com.br.ujungo.Service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by duh on 6/24/17.
 */

public class AccountPresenter {

    //private static String BASE_URL = "http://10.0.3.2/";
    private static String BASE_URL = "http://easytvteste2017.esy.es/";

    private UserModel userModel;
    private AccountFragment accountFragment;

    public AccountPresenter(AccountFragment accountFragment) {
        this.accountFragment = accountFragment;
    }

    public void LoadData(String id) {

        accountFragment.execLoading();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<UserModel> call = service.load(id);
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {

                if (response.isSuccessful()) {
                    userModel = response.body();

                    accountFragment.loadData(
                            userModel.getName(),
                            userModel.getEmail(),
                            userModel.getPhone(),
                            userModel.getAvatar());

                    accountFragment.stopLoading();

                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                accountFragment.stopLoading();
            }
        });
    }

}
