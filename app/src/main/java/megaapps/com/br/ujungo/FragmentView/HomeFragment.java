package megaapps.com.br.ujungo.FragmentView;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import megaapps.com.br.ujungo.ActivityView.HomeActivity;
import megaapps.com.br.ujungo.Extendables.MyFragment;
import megaapps.com.br.ujungo.R;
import megaapps.com.br.ujungo.Util.CustomFontButton;

public class HomeFragment extends MyFragment {

    @BindView(R.id.btnOne)
    CustomFontButton btnOne;
    @BindView(R.id.btnTwo)
    CustomFontButton btnTwo;
    @BindView(R.id.btnTree)
    CustomFontButton btnTree;
    @BindView(R.id.openDrawer)
    Button openDrawer;
    Unbinder unbinder;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((HomeActivity) getActivity()).closeDrawer();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnOne, R.id.btnTwo, R.id.btnTree, R.id.openDrawer})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.btnOne:
                HomeActivity homeactivity = (HomeActivity) getActivity();
                RequestDeliveryFragment requestDeliveryFragment = new RequestDeliveryFragment();
                FragmentTransaction ftDeliveryMan = homeactivity.getSupportFragmentManager().beginTransaction();
                ftDeliveryMan.addToBackStack("home_app");
                ftDeliveryMan.replace(R.id.frContent, requestDeliveryFragment);
                ftDeliveryMan.commit();
                break;

            case R.id.btnTwo:
                homeactivity = (HomeActivity) getActivity();
                WantDeliveryManFragment wantDeliveryManFragment = new WantDeliveryManFragment();
                FragmentTransaction ftIWantDeliveryMan = homeactivity.getSupportFragmentManager().beginTransaction();
                ftIWantDeliveryMan.addToBackStack("want_delivery_man");
                ftIWantDeliveryMan.replace(R.id.frContent, wantDeliveryManFragment);
                ftIWantDeliveryMan.commit();
                break;

            case R.id.btnTree:
                homeactivity = (HomeActivity) getActivity();
                SelectPubFragment selectPub = new SelectPubFragment();
                FragmentTransaction ftTravel = homeactivity.getSupportFragmentManager().beginTransaction();
                ftTravel.addToBackStack("select_pub");
                ftTravel.replace(R.id.frContent, selectPub);
                ftTravel.commit();
                break;

            case R.id.openDrawer:
                ((HomeActivity) getActivity()).openDrawer();
                break;

        }
    }
}
