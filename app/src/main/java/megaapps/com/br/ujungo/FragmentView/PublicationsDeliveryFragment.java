package megaapps.com.br.ujungo.FragmentView;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import megaapps.com.br.ujungo.ActivityView.HomeActivity;
import megaapps.com.br.ujungo.Adapter.PublicationDeliveryAdapter;
import megaapps.com.br.ujungo.Extendables.MyFragment;
import megaapps.com.br.ujungo.Model.RequestDeliveryModel;
import megaapps.com.br.ujungo.Presenter.PublicationsDeliveryPresenter;
import megaapps.com.br.ujungo.R;

/**
 * Created by mariliavilas on 04/07/17.
 */

public class PublicationsDeliveryFragment extends MyFragment {

    @BindView(R.id.layLoading)
    RelativeLayout layLoading;
    @BindView(R.id.relativeData)
    RelativeLayout relativeData;
    @BindView(R.id.rcDeliveryMan)
    RecyclerView rcDeliveryMan;
    Unbinder unbinder;

    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    PublicationDeliveryAdapter publicationDeliveryAdapter;
    @BindView(R.id.openDrawer)
    Button openDrawer;

    private PublicationsDeliveryPresenter publicationsDeliveryPresenter;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_publications_deliveryman_request, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        publicationsDeliveryPresenter = new PublicationsDeliveryPresenter(this);
        publicationsDeliveryPresenter.getDataDelManRequest();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void loadDelManRequest(List<RequestDeliveryModel> requestDeliveryList) {

        stopLoading();
        layoutManager = new LinearLayoutManager(getContext());
        rcDeliveryMan.setLayoutManager(layoutManager);
        adapter = new PublicationDeliveryAdapter(getContext(), requestDeliveryList);
        rcDeliveryMan.setAdapter(adapter);

    }

    public void execLoading() {
        layLoading.setVisibility(View.VISIBLE);
        relativeData.setVisibility(View.GONE);
    }

    public void stopLoading() {
        layLoading.setVisibility(View.GONE);
        relativeData.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.openDrawer)
    public void onViewClicked() {
        ((HomeActivity) getActivity()).openDrawer();
    }
}
