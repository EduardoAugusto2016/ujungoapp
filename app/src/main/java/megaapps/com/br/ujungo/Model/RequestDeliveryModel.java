package megaapps.com.br.ujungo.Model;

/**
 * Created by duh on 6/24/17.
 */

public class RequestDeliveryModel {

    private String id;
    private String size;
    private String tittle;
    private String info;
    private String addressePub;
    private String numberPub;
    private String neighborhoodPub;
    private String cityPub;
    private String statePub;
    private String cepPub;
    private String addresseePub;
    private String datePub;
    private String valuePub;
    private String image;
    private String resultCode;
    private String accept;

    public RequestDeliveryModel(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getAddressePub() {
        return addressePub;
    }

    public void setAddressePub(String addressePub) {
        this.addressePub = addressePub;
    }

    public String getNumberPub() {
        return numberPub;
    }

    public void setNumberPub(String numberPub) {
        this.numberPub = numberPub;
    }

    public String getNeighborhoodPub() {
        return neighborhoodPub;
    }

    public void setNeighborhoodPub(String neighborhoodPub) {
        this.neighborhoodPub = neighborhoodPub;
    }

    public String getCityPub() {
        return cityPub;
    }

    public void setCityPub(String cityPub) {
        this.cityPub = cityPub;
    }

    public String getStatePub() {
        return statePub;
    }

    public void setStatePub(String statePub) {
        this.statePub = statePub;
    }

    public String getCepPub() {
        return cepPub;
    }

    public void setCepPub(String cepPub) {
        this.cepPub = cepPub;
    }

    public String getAddresseePub() {
        return addresseePub;
    }

    public void setAddresseePub(String addresseePub) {
        this.addresseePub = addresseePub;
    }

    public String getDatePub() {
        return datePub;
    }

    public void setDatePub(String datePub) {
        this.datePub = datePub;
    }

    public String getValuePub() {
        return valuePub;
    }

    public void setValuePub(String valuePub) {
        this.valuePub = valuePub;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAccept() {
        return accept;
    }

    public void setAccept(String accept) {
        this.accept = accept;
    }
}
