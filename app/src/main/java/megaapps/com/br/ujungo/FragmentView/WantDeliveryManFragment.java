package megaapps.com.br.ujungo.FragmentView;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import megaapps.com.br.ujungo.ActivityView.HomeActivity;
import megaapps.com.br.ujungo.Extendables.MyFragment;
import megaapps.com.br.ujungo.Presenter.WantDeliveryManPresenter;
import megaapps.com.br.ujungo.R;
import megaapps.com.br.ujungo.Util.CustomFontButton;
import megaapps.com.br.ujungo.Util.CustomFontEditText;
import megaapps.com.br.ujungo.Util.CustomFontTextView;
import megaapps.com.br.ujungo.Util.DatePickerFragment;

/**
 * Created by duh on 6/25/17.
 */

public class WantDeliveryManFragment extends MyFragment {

    @BindView(R.id.TittleLoading)
    CustomFontTextView TittleLoading;
    @BindView(R.id.txtVehicle)
    CustomFontTextView txtVehicle;
    @BindView(R.id.txtOne)
    CustomFontTextView txtOne;
    @BindView(R.id.txtTwo)
    CustomFontTextView txtTwo;
    @BindView(R.id.txtTree)
    CustomFontTextView txtTree;
    @BindView(R.id.txtFour)
    CustomFontTextView txtFour;
    @BindView(R.id.txtDescTittlePub)
    CustomFontTextView txtDescTittlePub;
    @BindView(R.id.editTittlePub)
    CustomFontEditText editTittlePub;
    @BindView(R.id.editInfo)
    CustomFontEditText editInfo;
    @BindView(R.id.editDate)
    CustomFontEditText editDate;
    @BindView(R.id.txtDescAddress)
    CustomFontTextView txtDescAddress;
    @BindView(R.id.editCityPub)
    CustomFontEditText editCityPub;
    @BindView(R.id.editCountryPub)
    CustomFontEditText editCountryPub;
    @BindView(R.id.btnSave)
    CustomFontButton btnSave;
    @BindView(R.id.input_layout_tittle)
    TextInputLayout inputLayoutTittle;
    @BindView(R.id.input_layout_data)
    TextInputLayout inputLayoutData;
    @BindView(R.id.input_layout_spinner)
    TextInputLayout inputLayoutSpinner;
    @BindView(R.id.input_layout_city_pub)
    TextInputLayout inputLayoutCityPub;
    @BindView(R.id.input_layout_country_pub)
    TextInputLayout inputLayoutCountryPub;
    @BindView(R.id.input_layout_desc)
    TextInputLayout input_layout_desc;
    @BindView(R.id.spinnerTravel)
    MaterialBetterSpinner spinnerTravel;
    @BindView(R.id.imgOne)
    ImageView imgOne;
    @BindView(R.id.imgTwo)
    ImageView imgTwo;
    @BindView(R.id.imgTree)
    ImageView imgTree;
    @BindView(R.id.imgFour)
    ImageView imgFour;
    @BindView(R.id.layItemOne)
    RelativeLayout layItemOne;
    @BindView(R.id.layItemTwo)
    RelativeLayout layItemTwo;
    @BindView(R.id.layItemTree)
    RelativeLayout layItemTree;
    @BindView(R.id.layItemFour)
    RelativeLayout layItemFour;
    @BindView(R.id.layItemOneInvisible)
    RelativeLayout layItemOneInvisible;
    @BindView(R.id.layItemTwoInvisible)
    RelativeLayout layItemTwoInvisible;
    @BindView(R.id.layItemTreeInvisible)
    RelativeLayout layItemTreeInvisible;
    @BindView(R.id.layItemFourInvisible)
    RelativeLayout layItemFourInvisible;
    Unbinder unbinder;
    @BindView(R.id.openDrawer)
    Button openDrawer;

    private WantDeliveryManPresenter wantDeliveryManPresenter;

    private static final String[] ITEMS = {"Nacional", "Internacional"};
    private String bagSize = "";
    private String id = "";

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_go_travel, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((HomeActivity) getActivity()).closeDrawer();

        pref = getContext().getSharedPreferences("login.conf", Context.MODE_PRIVATE);
        id = pref.getString("uid", "");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, ITEMS);
        spinnerTravel.setAdapter(adapter);
        wantDeliveryManPresenter = new WantDeliveryManPresenter(this);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({
            R.id.editDate,
            R.id.layItemOneInvisible,
            R.id.layItemTwoInvisible,
            R.id.layItemTreeInvisible,
            R.id.layItemFourInvisible,
            R.id.btnSave,
            R.id.openDrawer
    })
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.editDate:

                FragmentManager fm = getActivity().getFragmentManager();
                DatePickerFragment dialog = new DatePickerFragment();
                dialog.show(fm, "dateOne");

                break;

            case R.id.layItemOneInvisible:
                bagSize = "pequeno";

                imgOne.setBackground(getResources().getDrawable(R.drawable.ic_bag_one_white));
                imgTwo.setBackground(getResources().getDrawable(R.drawable.ic_bag_two_gray));
                imgTree.setBackground(getResources().getDrawable(R.drawable.ic_bag_tree_gray));
                imgFour.setBackground(getResources().getDrawable(R.drawable.ic_bag_four_gray));

                txtOne.setTextColor(getResources().getColor(R.color.white));
                txtTwo.setTextColor(getResources().getColor(R.color.marginGray));
                txtTree.setTextColor(getResources().getColor(R.color.marginGray));
                txtFour.setTextColor(getResources().getColor(R.color.marginGray));

                layItemOne.setBackground(getResources().getDrawable(R.drawable.background_item_blue));
                layItemTwo.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
                layItemTree.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
                layItemFour.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));

                break;
            case R.id.layItemTwoInvisible:
                bagSize = "médio";

                imgOne.setBackground(getResources().getDrawable(R.drawable.ic_bag_one_gray));
                imgTwo.setBackground(getResources().getDrawable(R.drawable.ic_bag_two_white));
                imgTree.setBackground(getResources().getDrawable(R.drawable.ic_bag_tree_gray));
                imgFour.setBackground(getResources().getDrawable(R.drawable.ic_bag_four_gray));

                txtTwo.setTextColor(getResources().getColor(R.color.white));
                txtOne.setTextColor(getResources().getColor(R.color.marginGray));
                txtTree.setTextColor(getResources().getColor(R.color.marginGray));
                txtFour.setTextColor(getResources().getColor(R.color.marginGray));

                layItemTwo.setBackground(getResources().getDrawable(R.drawable.background_item_blue));
                layItemOne.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
                layItemTree.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
                layItemFour.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));

                break;
            case R.id.layItemTreeInvisible:
                bagSize = "grande";

                imgOne.setBackground(getResources().getDrawable(R.drawable.ic_bag_one_gray));
                imgTwo.setBackground(getResources().getDrawable(R.drawable.ic_bag_two_gray));
                imgTree.setBackground(getResources().getDrawable(R.drawable.ic_bag_tree_white));
                imgFour.setBackground(getResources().getDrawable(R.drawable.ic_bag_four_gray));

                txtTree.setTextColor(getResources().getColor(R.color.white));
                txtOne.setTextColor(getResources().getColor(R.color.marginGray));
                txtTwo.setTextColor(getResources().getColor(R.color.marginGray));
                txtFour.setTextColor(getResources().getColor(R.color.marginGray));

                layItemTree.setBackground(getResources().getDrawable(R.drawable.background_item_blue));
                layItemOne.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
                layItemTwo.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
                layItemFour.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));

                break;
            case R.id.layItemFourInvisible:
                bagSize = "enorme";

                imgOne.setBackground(getResources().getDrawable(R.drawable.ic_bag_one_gray));
                imgTwo.setBackground(getResources().getDrawable(R.drawable.ic_bag_two_gray));
                imgTree.setBackground(getResources().getDrawable(R.drawable.ic_bag_tree_gray));
                imgFour.setBackground(getResources().getDrawable(R.drawable.ic_bag_four_white));

                txtFour.setTextColor(getResources().getColor(R.color.white));
                txtOne.setTextColor(getResources().getColor(R.color.marginGray));
                txtTwo.setTextColor(getResources().getColor(R.color.marginGray));
                txtTree.setTextColor(getResources().getColor(R.color.marginGray));

                layItemFour.setBackground(getResources().getDrawable(R.drawable.background_item_blue));
                layItemOne.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
                layItemTwo.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
                layItemTree.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));

                break;

            case R.id.btnSave:
                wantDeliveryManPresenter.getDataWantDeliveryMan(
                        id,
                        bagSize,
                        editTittlePub.getText().toString(),
                        editInfo.getText().toString(),
                        editDate.getText().toString(),
                        spinnerTravel.getText().toString(),
                        editCityPub.getText().toString(),
                        editCountryPub.getText().toString()
                );

                break;

            case R.id.openDrawer:
                ((HomeActivity) getActivity()).openDrawer();

                break;
        }
    }

    public void emptyBagSize() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(WantDeliveryManFragment.this.getString(R.string.dialogAtention));
        builder.setIcon(R.drawable.ic_warning);
        builder.setMessage(this.getString(R.string.dialogNoSelectBag));
        builder.show();

    }

    public void emptyTittle() {

        inputLayoutTittle.setError(getText(R.string.emptyTittle));
        editTittlePub.requestFocus();

    }

    public void emptyInfo() {

        input_layout_desc.setError(getText(R.string.emptyInfo));
        editInfo.requestFocus();

    }

    public void emptyData() {

        inputLayoutData.setError(getText(R.string.emptyDate));
        editDate.requestFocus();

    }

    public void emptyTravelType() {
        inputLayoutSpinner.setError(getText(R.string.emptyTravelType));
        spinnerTravel.requestFocus();
    }

    public void emptyCity() {
        inputLayoutCityPub.setError(getText(R.string.emptyCity));
        editCityPub.requestFocus();
    }

    public void emptyCountry() {
        inputLayoutCountryPub.setError(getText(R.string.emptyCity));
        editCountryPub.requestFocus();
    }

    public void createOK() {

        imgOne.setBackground(getResources().getDrawable(R.drawable.ic_bag_one_gray));
        imgTwo.setBackground(getResources().getDrawable(R.drawable.ic_bag_two_gray));
        imgTree.setBackground(getResources().getDrawable(R.drawable.ic_bag_tree_gray));
        imgFour.setBackground(getResources().getDrawable(R.drawable.ic_bag_four_gray));

        txtTwo.setTextColor(getResources().getColor(R.color.marginGray));
        txtOne.setTextColor(getResources().getColor(R.color.marginGray));
        txtTree.setTextColor(getResources().getColor(R.color.marginGray));
        txtFour.setTextColor(getResources().getColor(R.color.marginGray));

        layItemTwo.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
        layItemOne.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
        layItemTree.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
        layItemFour.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));

        editTittlePub.setText("");
        editDate.setText("");
        editInfo.setText("");
        spinnerTravel.setText("");
        editCityPub.setText("");
        editCountryPub.setText("");

        Toast.makeText(getContext(), R.string.requestsuccess, Toast.LENGTH_LONG).show();
    }
}
