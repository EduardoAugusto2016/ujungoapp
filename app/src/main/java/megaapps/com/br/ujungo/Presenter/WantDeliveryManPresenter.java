package megaapps.com.br.ujungo.Presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import megaapps.com.br.ujungo.Model.WantDeliveryManModel;
import megaapps.com.br.ujungo.Service.APIService;
import megaapps.com.br.ujungo.FragmentView.WantDeliveryManFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by duh on 7/6/17.
 */

public class WantDeliveryManPresenter {

    //private static String BASE_URL = "http://10.0.3.2/";
    private static String BASE_URL = "http://easytvteste2017.esy.es/";

    private WantDeliveryManModel wantDeliveryManModel;

    WantDeliveryManFragment wantDeliveryManFragment;

    public WantDeliveryManPresenter(WantDeliveryManFragment wantDeliveryManFragment) {
        this.wantDeliveryManFragment = wantDeliveryManFragment;
    }

    public void getDataWantDeliveryMan(String id, String bagSize, String tittle, String info, String data, String travelType, String city,  String country) {

        if(bagSize.isEmpty() || tittle.isEmpty() || data.isEmpty() || travelType.isEmpty() || city.isEmpty() || country.isEmpty()){

            if(bagSize.isEmpty()){

                wantDeliveryManFragment.emptyBagSize();
                return;
            }
            if(tittle.isEmpty()){
                wantDeliveryManFragment.emptyTittle();
                return;
            }
            if(info.isEmpty()){
                wantDeliveryManFragment.emptyInfo();
                return;
            }
            if(data.isEmpty()){
                wantDeliveryManFragment.emptyData();
                return;
            }
            if(travelType.isEmpty()){
                wantDeliveryManFragment.emptyTravelType();
                return;
            }
            if(city.isEmpty()){
                wantDeliveryManFragment.emptyCity();
                return;
            }
            if(country.isEmpty()){
                wantDeliveryManFragment.emptyCountry();
                return;
            }

        }else{

            getDataCreateWantDeliveryMan(id, bagSize, tittle, info, data, travelType, city, country);

        }
    }

    private void getDataCreateWantDeliveryMan(String id, String bagSize, String tittle, String info , String data, String travelType, String city, String country) {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<WantDeliveryManModel> call = service.createWantDeliveryMan(id, bagSize, tittle, info, data, travelType, city, country);
        call.enqueue(new Callback<WantDeliveryManModel>() {
            @Override
            public void onResponse(Call<WantDeliveryManModel> call, Response<WantDeliveryManModel> response) {

                if(response.isSuccessful()){
                    wantDeliveryManModel= response.body();

                    if(wantDeliveryManModel.getResultCode().equals("OK")){

                        wantDeliveryManFragment.createOK();

                    }
                }
            }

            @Override
            public void onFailure(Call<WantDeliveryManModel> call, Throwable t) {

            }
        });
    }
}
