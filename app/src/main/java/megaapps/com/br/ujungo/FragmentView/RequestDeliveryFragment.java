package megaapps.com.br.ujungo.FragmentView;

import android.Manifest;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.otto.Subscribe;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import megaapps.com.br.ujungo.ActivityView.HomeActivity;
import megaapps.com.br.ujungo.Extendables.MyFragment;
import megaapps.com.br.ujungo.Model.RequestDeliveryModel;
import megaapps.com.br.ujungo.R;
import megaapps.com.br.ujungo.Service.APIService;
import megaapps.com.br.ujungo.Util.ActivityResultBus;
import megaapps.com.br.ujungo.Util.ActivityResultEvent;
import megaapps.com.br.ujungo.Util.DatePickerFragment;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RequestDeliveryFragment extends MyFragment {

    private static String BASE_URL = "http://10.0.3.2/";
    @BindView(R.id.openDrawer)
    Button openDrawer;

    private RequestDeliveryModel requestDeliveryModel;

    @BindView(R.id.itemImage)
    ImageView itemImage;
    private String id = "1";
    private String size = "";

    @BindView(R.id.imgOne)
    ImageView imgOne;
    @BindView(R.id.imgTwo)
    ImageView imgTwo;
    @BindView(R.id.imgTree)
    ImageView imgTree;
    @BindView(R.id.imgFour)
    ImageView imgFour;
    @BindView(R.id.editTittlePub)
    EditText editTittlePub;
    @BindView(R.id.editInfo)
    EditText editInfo;
    @BindView(R.id.editAddressPub)
    EditText editAddressPub;
    @BindView(R.id.editNumberPub)
    EditText editNumberPub;
    @BindView(R.id.editNeighborhoodPub)
    EditText editNeighborhoodPub;
    @BindView(R.id.editCityPub)
    EditText editCityPub;
    @BindView(R.id.editStatePub)
    EditText editStatePub;
    @BindView(R.id.editCepPub)
    EditText editCepPub;
    @BindView(R.id.editAddressee)
    EditText editAddressee;
    @BindView(R.id.editDate)
    EditText editDate;
    @BindView(R.id.editValue)
    EditText editValue;
    @BindView(R.id.input_layout_tittle)
    TextInputLayout inputLayoutTittle;
    @BindView(R.id.input_layout_desc)
    TextInputLayout inputLayoutDesc;
    @BindView(R.id.input_layout_address_pub)
    TextInputLayout inputLayoutAddressPub;
    @BindView(R.id.input_layout_number_pub)
    TextInputLayout inputLayoutNumberPub;
    @BindView(R.id.input_layout_neighborhood_pub)
    TextInputLayout inputLayoutNeighborhoodPub;
    @BindView(R.id.input_layout_city_pub)
    TextInputLayout inputLayoutCityPub;
    @BindView(R.id.input_layout_state_pub)
    TextInputLayout inputLayoutStatePub;
    @BindView(R.id.input_layout_cep_pub)
    TextInputLayout inputLayoutCepPub;
    @BindView(R.id.input_layout_addressee)
    TextInputLayout inputLayoutAddressee;
    @BindView(R.id.input_layout_date)
    TextInputLayout inputLayoutDate;
    @BindView(R.id.input_layout_value)
    TextInputLayout inputLayoutValue;
    @BindView(R.id.txtOne)
    TextView txtOne;
    @BindView(R.id.txtTwo)
    TextView txtTwo;
    @BindView(R.id.txtTree)
    TextView txtTree;
    @BindView(R.id.txtFour)
    TextView txtFour;
    @BindView(R.id.layItemOne)
    RelativeLayout layItemOne;
    @BindView(R.id.layItemTwo)
    RelativeLayout layItemTwo;
    @BindView(R.id.layItemTree)
    RelativeLayout layItemTree;
    @BindView(R.id.layItemFour)
    RelativeLayout layItemFour;
    @BindView(R.id.layItemOneInvisible)
    RelativeLayout layItemOneInvisible;
    @BindView(R.id.layItemTwoInvisible)
    RelativeLayout layItemTwoInvisible;
    @BindView(R.id.layItemTreeInvisible)
    RelativeLayout layItemTreeInvisible;
    @BindView(R.id.layItemFourInvisible)
    RelativeLayout layItemFourInvisible;
    @BindView(R.id.btnSave)
    Button btnSave;
    Unbinder unbinder;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    String mediaPath;

    public static final int CODE_GALLERY = 1;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_request_delivery, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((HomeActivity) getActivity()).closeDrawer();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.itemImage, R.id.editDate, R.id.layItemOneInvisible, R.id.layItemTwoInvisible, R.id.layItemTreeInvisible, R.id.layItemFourInvisible, R.id.btnSave, R.id.openDrawer})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.itemImage:
                openCam();
                break;

            case R.id.editDate:
                FragmentManager fm = getActivity().getFragmentManager();
                DatePickerFragment dialog = new DatePickerFragment();
                dialog.show(fm, "dateOne");

                break;

            case R.id.layItemOneInvisible:
                size = "pequeno";

                txtOne.setTextColor(getResources().getColor(R.color.white));
                txtTwo.setTextColor(getResources().getColor(R.color.marginGray));
                txtTree.setTextColor(getResources().getColor(R.color.marginGray));
                txtFour.setTextColor(getResources().getColor(R.color.marginGray));

                imgOne.setBackground(getResources().getDrawable(R.drawable.ic_item_one_white));
                imgTwo.setBackground(getResources().getDrawable(R.drawable.ic_item_two_gray));
                imgTree.setBackground(getResources().getDrawable(R.drawable.ic_item_tree_gray));
                imgFour.setBackground(getResources().getDrawable(R.drawable.ic_item_four_gray));

                layItemOne.setBackground(getResources().getDrawable(R.drawable.background_item_blue));
                layItemTwo.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
                layItemTree.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
                layItemFour.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));

                break;
            case R.id.layItemTwoInvisible:
                size = "medio";

                txtTwo.setTextColor(getResources().getColor(R.color.white));
                txtOne.setTextColor(getResources().getColor(R.color.marginGray));
                txtTree.setTextColor(getResources().getColor(R.color.marginGray));
                txtFour.setTextColor(getResources().getColor(R.color.marginGray));

                imgOne.setBackground(getResources().getDrawable(R.drawable.ic_item_one_gray));
                imgTwo.setBackground(getResources().getDrawable(R.drawable.ic_item_two_white));
                imgTree.setBackground(getResources().getDrawable(R.drawable.ic_item_tree_gray));
                imgFour.setBackground(getResources().getDrawable(R.drawable.ic_item_four_gray));

                layItemTwo.setBackground(getResources().getDrawable(R.drawable.background_item_blue));
                layItemOne.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
                layItemTree.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
                layItemFour.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));

                break;
            case R.id.layItemTreeInvisible:
                size = "grande";

                txtTree.setTextColor(getResources().getColor(R.color.white));
                txtOne.setTextColor(getResources().getColor(R.color.marginGray));
                txtTwo.setTextColor(getResources().getColor(R.color.marginGray));
                txtFour.setTextColor(getResources().getColor(R.color.marginGray));

                imgOne.setBackground(getResources().getDrawable(R.drawable.ic_item_one_gray));
                imgTwo.setBackground(getResources().getDrawable(R.drawable.ic_item_two_gray));
                imgTree.setBackground(getResources().getDrawable(R.drawable.ic_item_tree_white));
                imgFour.setBackground(getResources().getDrawable(R.drawable.ic_item_four_gray));

                layItemTree.setBackground(getResources().getDrawable(R.drawable.background_item_blue));
                layItemOne.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
                layItemTwo.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
                layItemFour.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));

                break;
            case R.id.layItemFourInvisible:
                size = "enorme";

                txtFour.setTextColor(getResources().getColor(R.color.white));
                txtOne.setTextColor(getResources().getColor(R.color.marginGray));
                txtTwo.setTextColor(getResources().getColor(R.color.marginGray));
                txtTree.setTextColor(getResources().getColor(R.color.marginGray));

                imgOne.setBackground(getResources().getDrawable(R.drawable.ic_item_one_gray));
                imgTwo.setBackground(getResources().getDrawable(R.drawable.ic_item_two_gray));
                imgTree.setBackground(getResources().getDrawable(R.drawable.ic_item_tree_gray));
                imgFour.setBackground(getResources().getDrawable(R.drawable.ic_item_four_white));

                layItemFour.setBackground(getResources().getDrawable(R.drawable.background_item_blue));
                layItemOne.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
                layItemTwo.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
                layItemTree.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));

                break;
            case R.id.btnSave:

                if (size.isEmpty() ||
                        editTittlePub.getText().toString().isEmpty() ||
                        editInfo.getText().toString().isEmpty() ||
                        editAddressPub.getText().toString().isEmpty() ||
                        editNumberPub.getText().toString().isEmpty() ||
                        editNeighborhoodPub.getText().toString().isEmpty() ||
                        editCityPub.getText().toString().isEmpty() ||
                        editStatePub.getText().toString().isEmpty() ||
                        editCepPub.getText().toString().isEmpty() ||
                        editAddressee.getText().toString().isEmpty() ||
                        editDate.getText().toString().isEmpty() ||
                        editValue.getText().toString().isEmpty()) {

                    if (size.isEmpty()) {
                        emptySize();
                        return;
                    }
                    if (editTittlePub.getText().toString().isEmpty()) {
                        emptyTittle();
                        return;
                    }
                    if (editInfo.getText().toString().isEmpty()) {
                        emptyinfo();
                        return;
                    }
                    if (editAddressPub.getText().toString().isEmpty()) {
                        emptyAddressPub();
                        return;
                    }
                    if (editNumberPub.getText().toString().isEmpty()) {
                        emptyNumberPub();
                        return;
                    }
                    if (editNeighborhoodPub.getText().toString().isEmpty()) {
                        emptyNeighborhoodPub();
                        return;
                    }
                    if (editCityPub.getText().toString().isEmpty()) {
                        emptyCityPub();
                        return;
                    }
                    if (editStatePub.getText().toString().isEmpty()) {
                        emptyStatePub();
                        return;
                    }
                    if (editCepPub.getText().toString().isEmpty()) {
                        emptyCepPub();
                        return;
                    }
                    if (editAddressee.getText().toString().isEmpty()) {
                        emptyAddresseePub();
                        return;
                    }
                    if (editDate.getText().toString().isEmpty()) {
                        emptyDatePub();
                        return;
                    }
                    if (editValue.getText().toString().isEmpty()) {
                        emptyValuePub();
                        return;
                    }
                } else {

                    uploadFile();

                }

                break;

            case R.id.openDrawer:
                ((HomeActivity) getActivity()).openDrawer();

                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        ActivityResultBus.getInstance().register(mActivityResultSubscriber);
    }

    @Override
    public void onStop() {
        super.onStop();
        ActivityResultBus.getInstance().unregister(mActivityResultSubscriber);
    }

    @Override
    public void onResume() {
        super.onResume();
        ActivityResultBus.getInstance();
    }

    private Object mActivityResultSubscriber = new Object() {
        @Subscribe
        public void onActivityResultReceived(ActivityResultEvent event) {
            int requestCode = event.getRequestCode();
            int resultCode = event.getResultCode();
            Intent data = event.getData();
            onActivityResult(requestCode, resultCode, data);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (data == null) {

                Toast.makeText(getContext(), R.string.noImageSelected, Toast.LENGTH_SHORT).show();

            } else {

                if (requestCode == CODE_GALLERY) {

                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContext().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    assert cursor != null;
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    mediaPath = cursor.getString(columnIndex);

                    Glide.with(getContext())
                            .load(mediaPath)
                            .into(itemImage);

                    cursor.close();

                }
            }

        } catch (Exception e) {
            Toast.makeText(getContext(), R.string.noImageSelected, Toast.LENGTH_LONG).show();
        }

    }

    private void openCam() {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2);

        } else if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);

        } else {
            Intent i = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            getActivity().startActivityForResult(i, CODE_GALLERY);
        }

    }

    public void emptySize() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(RequestDeliveryFragment.this.getString(R.string.dialogAtention));
        builder.setIcon(R.drawable.ic_warning);
        builder.setMessage(this.getString(R.string.dialogNoSelectItem));
        builder.show();
    }

    public void emptyTittle() {
        inputLayoutTittle.setError(getText(R.string.emptyTittle));
        editTittlePub.requestFocus();
    }

    public void emptyinfo() {
        inputLayoutDesc.setError(getText(R.string.emptyInfo));
        editInfo.requestFocus();
    }

    public void emptyAddressPub() {
        inputLayoutAddressPub.setError(getText(R.string.emptyAddress));
        editAddressPub.requestFocus();
    }

    public void emptyNumberPub() {
        inputLayoutNumberPub.setError(getText(R.string.emptyNumber));
        editNumberPub.requestFocus();
    }

    public void emptyNeighborhoodPub() {
        inputLayoutNeighborhoodPub.setError(getText(R.string.emptyNeighborhood));
        editNeighborhoodPub.requestFocus();
    }

    public void emptyCityPub() {
        inputLayoutCityPub.setError(getText(R.string.emptyCity));
        editCityPub.requestFocus();
    }

    public void emptyStatePub() {
        inputLayoutStatePub.setError(getText(R.string.emptyState));
        editStatePub.requestFocus();
    }

    public void emptyCepPub() {
        inputLayoutCepPub.setError(getText(R.string.emptyCep));
        editCepPub.requestFocus();
    }

    public void emptyAddresseePub() {
        inputLayoutAddressee.setError(getText(R.string.emptyName));
        editAddressee.requestFocus();
    }

    public void emptyDatePub() {
        inputLayoutDate.setError(getText(R.string.emptyDate));
        editDate.requestFocus();
    }

    public void emptyValuePub() {
        inputLayoutValue.setError(getText(R.string.emptyValue));
        editValue.requestFocus();
    }

    private void uploadFile() {

        pref = getContext().getSharedPreferences("login.conf", Context.MODE_PRIVATE);
        id = pref.getString("uid", "");

        File file = new File(mediaPath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), requestBody);

        RequestBody sizeText = RequestBody.create(MediaType.parse("text/plain"), size);
        RequestBody tiitle = RequestBody.create(MediaType.parse("text/plain"), editTittlePub.getText().toString());
        RequestBody info = RequestBody.create(MediaType.parse("text/plain"), editInfo.getText().toString());
        RequestBody address = RequestBody.create(MediaType.parse("text/plain"), editAddressPub.getText().toString());
        RequestBody number = RequestBody.create(MediaType.parse("text/plain"), editNumberPub.getText().toString());
        RequestBody neighborhood = RequestBody.create(MediaType.parse("text/plain"), editNeighborhoodPub.getText().toString());
        RequestBody city = RequestBody.create(MediaType.parse("text/plain"), editCityPub.getText().toString());
        RequestBody state = RequestBody.create(MediaType.parse("text/plain"), editStatePub.getText().toString());
        RequestBody cep = RequestBody.create(MediaType.parse("text/plain"), editCepPub.getText().toString());
        RequestBody addressee = RequestBody.create(MediaType.parse("text/plain"), editAddressee.getText().toString());
        RequestBody date = RequestBody.create(MediaType.parse("text/plain"), editDate.getText().toString());
        RequestBody value = RequestBody.create(MediaType.parse("text/plain"), editValue.getText().toString());

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<RequestDeliveryModel> call = service.createRequestDelivery(
                fileToUpload,
                id,
                sizeText,
                tiitle,
                info,
                address,
                number,
                neighborhood,
                city,
                state,
                cep,
                addressee,
                date,
                value
        );
        call.enqueue(new Callback<RequestDeliveryModel>() {
            @Override
            public void onResponse(Call<RequestDeliveryModel> call, Response<RequestDeliveryModel> response) {


                if (response.isSuccessful()) {
                    requestDeliveryModel = response.body();

                    if (requestDeliveryModel.getResultCode().equals("OK")) {

                        //         stopLoading();

                        createOk();


                    } else {

                    }
                }


            }

            @Override
            public void onFailure(Call<RequestDeliveryModel> call, Throwable t) {

            }
        });

    }

    public void createOk() {

        editTittlePub.setText("");
        editInfo.setText("");
        editAddressPub.setText("");
        editNumberPub.setText("");
        editNeighborhoodPub.setText("");
        editCityPub.setText("");
        editStatePub.setText("");
        editCepPub.setText("");
        editAddressee.setText("");
        editDate.setText("");
        editValue.setText("");

        txtOne.setTextColor(getResources().getColor(R.color.marginGray));
        txtTwo.setTextColor(getResources().getColor(R.color.marginGray));
        txtTree.setTextColor(getResources().getColor(R.color.marginGray));
        txtFour.setTextColor(getResources().getColor(R.color.marginGray));

        imgOne.setBackground(getResources().getDrawable(R.drawable.ic_item_one_gray));
        imgTwo.setBackground(getResources().getDrawable(R.drawable.ic_item_two_gray));
        imgTree.setBackground(getResources().getDrawable(R.drawable.ic_item_tree_gray));
        imgFour.setBackground(getResources().getDrawable(R.drawable.ic_item_four_gray));

        layItemOne.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
        layItemTwo.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
        layItemTree.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));
        layItemFour.setBackground(getResources().getDrawable(R.drawable.background_item_invisible));

        Glide.with(getContext())
                .load(R.drawable.bc_imagem)
                .into(itemImage);

        Toast.makeText(getContext(), R.string.requestsuccess, Toast.LENGTH_LONG).show();
    }


}
