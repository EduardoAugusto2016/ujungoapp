package megaapps.com.br.ujungo.Presenter;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import megaapps.com.br.ujungo.FragmentView.MyTravelFragment;
import megaapps.com.br.ujungo.Model.PublicationsTravelModel;
import megaapps.com.br.ujungo.Model.WantDeliveryManModel;
import megaapps.com.br.ujungo.Service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mariliavilas on 09/09/17.
 */

public class MyTravelPresenter {

    //private static String BASE_URL = "http://10.0.3.2/";
    private static String BASE_URL = "http://easytvteste2017.esy.es/";
    private MyTravelFragment myTravelFragment;

    private List<WantDeliveryManModel> requestDeliveryModel;
    private PublicationsTravelModel publicationsTravelModel;

    public MyTravelPresenter(MyTravelFragment myTravelFragment) {
        this.myTravelFragment = myTravelFragment;
    }

    public void load(String id) {

        myTravelFragment.execLoading();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<List<WantDeliveryManModel>> call = service.loadMyTravel(id);
        call.enqueue(new Callback<List<WantDeliveryManModel>>() {
            @Override
            public void onResponse(Call<List<WantDeliveryManModel>> call, Response<List<WantDeliveryManModel>> response) {

                if (response.isSuccessful()) {

                    requestDeliveryModel = response.body();
                    for(int i =0; i < requestDeliveryModel.size() ; i++){
                        myTravelFragment.loadMyTravel(requestDeliveryModel);
                    }
                }
            }
            @Override
            public void onFailure(Call<List<WantDeliveryManModel>> call, Throwable t) {
                myTravelFragment.stopLoading();
            }
        });
    }

    public void deleteTravel(String id) {

        myTravelFragment.execLoading();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<PublicationsTravelModel> call = service.deleteMyTravel(id);
        call.enqueue(new Callback<PublicationsTravelModel>() {
            @Override
            public void onResponse(Call<PublicationsTravelModel> call, Response<PublicationsTravelModel> response) {

                if (response.isSuccessful()) {

                    publicationsTravelModel = response.body();

                    if(publicationsTravelModel.getResultCode().equals("OK")){

                        myTravelFragment.load();
                        myTravelFragment.deleteOK();

                    }
                }
            }
            @Override
            public void onFailure(Call<PublicationsTravelModel> call, Throwable t) {
                myTravelFragment.stopLoading();
                Log.i("error_travel", t.getMessage());
            }
        });
    }
}