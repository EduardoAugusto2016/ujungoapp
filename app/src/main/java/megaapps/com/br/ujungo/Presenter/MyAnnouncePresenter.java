package megaapps.com.br.ujungo.Presenter;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import megaapps.com.br.ujungo.FragmentView.MyAnnounceFragment;
import megaapps.com.br.ujungo.Model.RequestDeliveryModel;
import megaapps.com.br.ujungo.Service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by duh on 9/9/17.
 */

public class MyAnnouncePresenter {

    //private static String BASE_URL = "http://10.0.3.2/";
    private static String BASE_URL = "http://easytvteste2017.esy.es/";

    private MyAnnounceFragment myAnnounceFragment;
    private List<RequestDeliveryModel> requestDeliveryModel ;
    private RequestDeliveryModel requestDelivery;

    public MyAnnouncePresenter(MyAnnounceFragment myAnnounceFragment) {
        this.myAnnounceFragment = myAnnounceFragment;
    }

    public void load(String id) {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<List<RequestDeliveryModel>> call = service.loadMyDelManRequest(id);
        call.enqueue(new Callback<List<RequestDeliveryModel>>() {
            @Override
            public void onResponse(Call<List<RequestDeliveryModel>> call, Response<List<RequestDeliveryModel>> response) {

                if (response.isSuccessful()) {

                    requestDeliveryModel = response.body();
                    for(int i =0; i < requestDeliveryModel.size() ; i++){
                        myAnnounceFragment.loadDelManRequest(requestDeliveryModel);
                    }
                }
            }
            @Override
            public void onFailure(Call<List<RequestDeliveryModel>> call, Throwable t) {
            }
        });
    }

    public void deleteAnnounce(String id) {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<RequestDeliveryModel> call = service.deleteMyAnnounce(id);
        call.enqueue(new Callback<RequestDeliveryModel>() {
            @Override
            public void onResponse(Call<RequestDeliveryModel> call, Response<RequestDeliveryModel> response) {

                if (response.isSuccessful()) {

                    requestDelivery = response.body();

                    if(requestDelivery.getResultCode().equals("OK")){

                        myAnnounceFragment.load();
                        myAnnounceFragment.deleteOK();

                    }

                }
            }
            @Override
            public void onFailure(Call<RequestDeliveryModel> call, Throwable t) {

                Log.i("error_announce", t.getMessage());

            }
        });
    }
}
