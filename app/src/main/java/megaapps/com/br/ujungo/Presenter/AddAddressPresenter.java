package megaapps.com.br.ujungo.Presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import megaapps.com.br.ujungo.Model.AddAddressModel;
import megaapps.com.br.ujungo.Service.APIService;
import megaapps.com.br.ujungo.FragmentView.AddAddressFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by duh on 7/9/17.
 */

public class AddAddressPresenter {

    //private static String BASE_URL = "http://10.0.3.2/";
    private static String BASE_URL = "http://easytvteste2017.esy.es/";

    private AddAddressModel addAddressModel;

    AddAddressFragment addAddressFragment;

    public AddAddressPresenter(AddAddressFragment addAddressFragment) {
        this.addAddressFragment = addAddressFragment;
    }

    public void getDataUpdateAddress(String id, String address, String number, String neighborhood, String cep, String city, String state, String country) {


        if (address.isEmpty() || number.isEmpty() || neighborhood.isEmpty() || cep.isEmpty() || city.isEmpty() || state.isEmpty() || country.isEmpty()) {

            if (address.isEmpty()) {
                addAddressFragment.emptyAddress();
                return;
            }
            if (number.isEmpty()) {
                addAddressFragment.emptyNumber();
                return;
            }
            if (neighborhood.isEmpty()) {
                addAddressFragment.emptyNeighborhood();
                return;
            }
            if (cep.isEmpty()) {
                addAddressFragment.emptyCep();
                return;
            }
            if (city.isEmpty()) {
                addAddressFragment.emptyCity();
                return;
            }
            if (state.isEmpty()) {
                addAddressFragment.emptyState();
                return;
            }
            if (country.isEmpty()) {
                addAddressFragment.emptyCountry();
                return;
            }

        } else {
            addAddressFragment.execLoading();
            updateAddress(id, address, number, neighborhood, cep, city, state, country);

        }

    }

    private void updateAddress(String id, String address, String number, String neighborhood, String cep, String city, String state, String country) {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<AddAddressModel> call = service.updateAddress(
                id,
                address,
                number,
                neighborhood,
                cep,
                city,
                state,
                country
        );

        call.enqueue(new Callback<AddAddressModel>() {
            @Override
            public void onResponse(Call<AddAddressModel> call, Response<AddAddressModel> response) {

                if (response.isSuccessful()) {

                    addAddressModel = response.body();

                    if (addAddressModel.getResultCode().equals("OK")) {
                        addAddressFragment.stopLoading();
                        addAddressFragment.updateOk();

                    }
                }
            }

            @Override
            public void onFailure(Call<AddAddressModel> call, Throwable t) {
                addAddressFragment.stopLoading();
            }
        });
    }

    public void load(String id) {

        addAddressFragment.execLoading();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<AddAddressModel> call = service.loadAddress(
                id

        );

        call.enqueue(new Callback<AddAddressModel>() {
            @Override
            public void onResponse(Call<AddAddressModel> call, Response<AddAddressModel> response) {

                if (response.isSuccessful()) {
                    addAddressFragment.stopLoading();
                    addAddressModel = response.body();

                    if (addAddressModel.getResultCode().equals("OK")) {

                        addAddressFragment.loadOK(
                                addAddressModel.getAddress(),
                                addAddressModel.getNumber(),
                                addAddressModel.getNeighborhood(),
                                addAddressModel.getZipcode(),
                                addAddressModel.getCity(),
                                addAddressModel.getState(),
                                addAddressModel.getCountry()
                        );

                    }
                }
            }

            @Override
            public void onFailure(Call<AddAddressModel> call, Throwable t) {
                addAddressFragment.stopLoading();

            }
        });

    }

}
