package megaapps.com.br.ujungo.Presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import megaapps.com.br.ujungo.Model.UserModel;
import megaapps.com.br.ujungo.Service.APIService;
import megaapps.com.br.ujungo.ActivityView.RegisterActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by duh on 6/21/17.
 */

public class RegisterPresenter {

    //private static String BASE_URL = "http://10.0.3.2/";
    private static String BASE_URL = "http://easytvteste2017.esy.es/";

    private UserModel userModel;
    private RegisterActivity registerActivity;

    public RegisterPresenter(RegisterActivity registerActivity) {
        this.registerActivity = registerActivity;
    }

    public void getDataRegister(String name, String email, String password, String passwordConf) {

        if(name.isEmpty() || email.isEmpty() || password.isEmpty() || passwordConf.isEmpty()){

            if(name.isEmpty()){
                registerActivity.nameEmpty();
                return;

            }
            if(email.isEmpty()){
                registerActivity.emailEmpty();
                return;

            }
            if(password.isEmpty()){
                registerActivity.passwordEmpty();
                return;

            }
            if(passwordConf.isEmpty()){
                registerActivity.passwordCEmpty();
                return;
            }
            if(!password.equals(passwordConf)){

                registerActivity.passwordNotEquals();
                return;

            }

        }else{

            registerActivity.execLoading();
            register(name, email, password);
        }
    }

    private void register(String name, String email, String password) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<UserModel> call = service.register(name, email, password);
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {

                if (response.isSuccessful()) {

                    userModel = response.body();

                    if (userModel.getResultCode().equals("OK")) {

                        registerActivity.registerOk();

                    }else if(userModel.getResultCode().equals("NO")){

                        registerActivity.stopLoading();

                        registerActivity.emailExists();

                    }
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                registerActivity.stopLoading();

            }
        });
    }
}
