package megaapps.com.br.ujungo.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import megaapps.com.br.ujungo.ActivityView.HomeActivity;
import megaapps.com.br.ujungo.FragmentView.MyAnnounceFragment;
import megaapps.com.br.ujungo.Model.RequestDeliveryModel;
import megaapps.com.br.ujungo.R;

/**
 * Created by duh on 9/9/17.
 */

public class MyAnnounceAdapter extends RecyclerView.Adapter<MyAnnounceAdapter.MyAnnounceRecyclerHolder> {

    private Context context;
    private List<RequestDeliveryModel> requestDeliveryList;
    private MyAnnounceFragment myAnnounceFragment;

    public MyAnnounceAdapter(Context context, List<RequestDeliveryModel> requestDeliveryList, MyAnnounceFragment myAnnounceFragment) {
        this.context = context;
        this.requestDeliveryList = requestDeliveryList;
        this.myAnnounceFragment = myAnnounceFragment;
    }

    @Override
    public MyAnnounceRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_my_announce, parent, false);

        return new MyAnnounceRecyclerHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyAnnounceRecyclerHolder holder, final int position) {

        MyAnnounceRecyclerHolder viewHolder = (MyAnnounceRecyclerHolder) holder;

        viewHolder.txtTittle.setText(requestDeliveryList.get(position).getTittle());
        viewHolder.txtCountry.setText(requestDeliveryList.get(position).getCityPub());
        viewHolder.txtCity.setText(requestDeliveryList.get(position).getStatePub());
        viewHolder.txtValue.setText("R$ " + requestDeliveryList.get(position).getValuePub());

        Glide.with(context).load(requestDeliveryList.get(position).getImage()).into(viewHolder.imgAnnounce);

        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myAnnounceFragment.deteleMyAnnounce(requestDeliveryList.get(position).getId());


            }
        });
    }

    @Override
    public int getItemCount() {
        return requestDeliveryList.size();
    }

    public class MyAnnounceRecyclerHolder extends RecyclerView.ViewHolder {

        public TextView txtTittle, txtCity, txtValue, txtCountry, txtAccept;
        public ImageView imgAnnounce;
        public ImageButton btnDelete;

        MyAnnounceRecyclerHolder(View itemView) {
            super(itemView);

            imgAnnounce = (ImageView) itemView.findViewById(R.id.imgAnnounce);
            txtTittle = (TextView) itemView.findViewById(R.id.txtTittle);
            txtCity = (TextView) itemView.findViewById(R.id.txtCity);
            txtValue = (TextView) itemView.findViewById(R.id.txtValue);
            txtCountry = (TextView) itemView.findViewById(R.id.txtCountry);
            txtAccept = (TextView) itemView.findViewById(R.id.txtAccept);
            txtValue = (TextView) itemView.findViewById(R.id.txtValue);
            btnDelete = (ImageButton) itemView.findViewById(R.id.btnDelete);


        }
    }
}
