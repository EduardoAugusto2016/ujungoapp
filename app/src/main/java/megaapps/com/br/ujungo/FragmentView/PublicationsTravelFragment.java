package megaapps.com.br.ujungo.FragmentView;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import megaapps.com.br.ujungo.ActivityView.HomeActivity;
import megaapps.com.br.ujungo.Adapter.PublicationTravelAdapter;
import megaapps.com.br.ujungo.Extendables.MyFragment;
import megaapps.com.br.ujungo.Model.PublicationsTravelModel;
import megaapps.com.br.ujungo.Presenter.PublicationsTravelPresenter;
import megaapps.com.br.ujungo.R;

/**
 * Created by mariliavilas on 04/07/17.
 */

public class PublicationsTravelFragment extends MyFragment {

    @BindView(R.id.layLoading)
    RelativeLayout layLoading;
    @BindView(R.id.relativeData)
    RelativeLayout relativeData;
    @BindView(R.id.rcTravel)
    RecyclerView rcTravel;
    Unbinder unbinder;

    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    PublicationTravelAdapter publicationTravelAdapter;
    @BindView(R.id.openDrawer)
    Button openDrawer;

    private PublicationsTravelPresenter publicationsTravelPresenter;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_publications_travel, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        publicationsTravelPresenter = new PublicationsTravelPresenter(this);
        publicationsTravelPresenter.getDataDelManRequest();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void loadDelManRequest(List<PublicationsTravelModel> requestDeliveryList) {

        stopLoading();

        if (!requestDeliveryList.isEmpty()) {

            layoutManager = new LinearLayoutManager(getContext());
            rcTravel.setLayoutManager(layoutManager);
            adapter = new PublicationTravelAdapter(getContext(), requestDeliveryList);
            rcTravel.setAdapter(adapter);

        }

    }

    public void execLoading() {
        layLoading.setVisibility(View.VISIBLE);
        relativeData.setVisibility(View.GONE);
    }

    public void stopLoading() {
        layLoading.setVisibility(View.GONE);
        relativeData.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.openDrawer)
    public void onViewClicked() {

        ((HomeActivity) getActivity()).openDrawer();
    }
}
