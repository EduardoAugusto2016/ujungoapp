package megaapps.com.br.ujungo.FragmentView;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import megaapps.com.br.ujungo.ActivityView.HomeActivity;
import megaapps.com.br.ujungo.Extendables.MyFragment;
import megaapps.com.br.ujungo.R;

/**
 * Created by duh on 7/8/17.
 */

public class SelectPubFragment extends MyFragment {

    @BindView(R.id.btnOne)
    Button btnOne;
    @BindView(R.id.btnTwo)
    Button btnTwo;
    Unbinder unbinder;
    @BindView(R.id.openDrawer)
    Button openDrawer;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_select_pub, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((HomeActivity) getActivity()).closeDrawer();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnOne, R.id.btnTwo, R.id.openDrawer})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnOne:

                HomeActivity homeactivity = (HomeActivity) getActivity();
                PublicationsDeliveryFragment publicationsDeliveryManRequestFragment = new PublicationsDeliveryFragment();
                FragmentTransaction ftDeliveryMan = homeactivity.getSupportFragmentManager().beginTransaction();
                ftDeliveryMan.addToBackStack("delivery_man");
                ftDeliveryMan.replace(R.id.frContent, publicationsDeliveryManRequestFragment);
                ftDeliveryMan.commit();
                break;

            case R.id.btnTwo:

                HomeActivity homeact = (HomeActivity) getActivity();
                PublicationsTravelFragment publicationsTravelFragment = new PublicationsTravelFragment();
                FragmentTransaction ftTravel = homeact.getSupportFragmentManager().beginTransaction();
                ftTravel.addToBackStack("travel");
                ftTravel.replace(R.id.frContent, publicationsTravelFragment);
                ftTravel.commit();
                break;

            case R.id.openDrawer:
                ((HomeActivity) getActivity()).openDrawer();
                break;
        }
    }
}
