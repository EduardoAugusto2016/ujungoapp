package megaapps.com.br.ujungo.FragmentView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import megaapps.com.br.ujungo.ActivityView.HomeActivity;
import megaapps.com.br.ujungo.Adapter.MyTravelAdapter;
import megaapps.com.br.ujungo.Extendables.MyFragment;
import megaapps.com.br.ujungo.Model.WantDeliveryManModel;
import megaapps.com.br.ujungo.Presenter.MyTravelPresenter;
import megaapps.com.br.ujungo.R;

public class MyTravelFragment extends MyFragment {

    @BindView(R.id.openDrawer)
    Button openDrawer;
    Unbinder unbinder;
    private RecyclerView rcMyTravel;
    private RecyclerView.Adapter myTravelAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private MyTravelPresenter myTravelPresenter;
    private String id;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_travel, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((HomeActivity) getActivity()).closeDrawer();

        rcMyTravel = (RecyclerView) view.findViewById(R.id.rcMyTravel);
        pref = getContext().getSharedPreferences("login.conf", Context.MODE_PRIVATE);
        id = pref.getString("uid", "");
        myTravelPresenter = new MyTravelPresenter(this);
        load();
    }

    public void load() {

        myTravelPresenter.load(id);
    }

    public void loadMyTravel(List<WantDeliveryManModel> wantDeliveryManModel) {

        if (!wantDeliveryManModel.isEmpty()) {

            layoutManager = new LinearLayoutManager(getContext());
            rcMyTravel.setLayoutManager(layoutManager);
            myTravelAdapter = new MyTravelAdapter(getContext(), wantDeliveryManModel, MyTravelFragment.this);
            rcMyTravel.setAdapter(myTravelAdapter);

        }else {

            Toast.makeText(getContext(), "Você não possui nenhuma viagem cadastrada!", Toast.LENGTH_LONG).show();

        }
    }

    public void execLoading() {
    }

    public void stopLoading() {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.openDrawer)
    public void onViewClicked() {

        ((HomeActivity) getActivity()).openDrawer();

    }

    public void deteleMyTravel(String id) {

        myTravelPresenter.deleteTravel(id);

    }

    public void deleteOK() {

        Toast.makeText(getContext(), "Viagem deletada com sucesso!", Toast.LENGTH_LONG).show();

    }
}
