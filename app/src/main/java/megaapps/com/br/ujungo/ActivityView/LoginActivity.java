package megaapps.com.br.ujungo.ActivityView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import megaapps.com.br.ujungo.Extendables.MyFragActivity;
import megaapps.com.br.ujungo.Presenter.LoginPresenter;
import megaapps.com.br.ujungo.R;
import megaapps.com.br.ujungo.Util.CustomFontButton;
import megaapps.com.br.ujungo.Util.CustomFontEditText;
import megaapps.com.br.ujungo.Util.CustomFontTextView;

public class LoginActivity extends MyFragActivity {

    SharedPreferences pref;
    SharedPreferences.Editor editor;


    @BindView(R.id.layLoading)
    RelativeLayout layLoading;
    @BindView(R.id.scrowData)
    ScrollView scrowData;
    @BindView(R.id.tittleLoading)
    CustomFontTextView tittleLoading;
    @BindView(R.id.editEmail)
    CustomFontEditText editEmail;
    @BindView(R.id.editPass)
    CustomFontEditText editPass;
    @BindView(R.id.btnLogin)
    CustomFontButton btnLogin;
    @BindView(R.id.txtForgot)
    CustomFontTextView txtForgot;
    @BindView(R.id.txtReg)
    CustomFontTextView txtReg;

    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        loginPresenter = new LoginPresenter(this);
    }

    @OnClick({R.id.btnLogin, R.id.txtForgot, R.id.txtReg})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                loginPresenter.getDataLogin(editEmail.getText().toString(), editPass.getText().toString());
                break;

            case R.id.txtForgot:
                startActivity(new Intent(LoginActivity.this, ForgotActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.txtReg:
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
        }
    }

    public void emailEmpty() {

        editEmail.requestFocus();
        editEmail.setError("Preencha o campo email!");

    }

    public void passwordEmpty() {

        editPass.requestFocus();
        editPass.setError("Preencha o campo senha!");

    }

    public void loginOk(String id, String name, String avatar) {

        pref = getApplicationContext().getSharedPreferences("login.conf", Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putString("uid", id);
        editor.putString("name", name);
        editor.putString("avatar", avatar);
        editor.putString("email", editEmail.getText().toString());
        editor.putString("password", editPass.getText().toString());
        editor.apply();

        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();

    }

    public void loginError() {
        Toast.makeText(this, "Usuário ou senha incorreta!", Toast.LENGTH_SHORT).show();
    }

    public void execLoading() {
        layLoading.setVisibility(View.VISIBLE);
        scrowData.setVisibility(View.GONE);
    }

    public void stopLoading() {

        layLoading.setVisibility(View.GONE);
        scrowData.setVisibility(View.VISIBLE);

    }

}
