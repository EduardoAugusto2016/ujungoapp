package megaapps.com.br.ujungo.FragmentView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import megaapps.com.br.ujungo.ActivityView.HomeActivity;
import megaapps.com.br.ujungo.Adapter.MyAnnounceAdapter;
import megaapps.com.br.ujungo.Extendables.MyFragment;
import megaapps.com.br.ujungo.Model.RequestDeliveryModel;
import megaapps.com.br.ujungo.Presenter.MyAnnouncePresenter;
import megaapps.com.br.ujungo.R;

public class MyAnnounceFragment extends MyFragment {

    @BindView(R.id.openDrawer)
    Button openDrawer;
    Unbinder unbinder;
    private MyAnnouncePresenter myAnnouncePresenter;

    private RecyclerView rcMyAnnunce;
    private RecyclerView.Adapter myAnnounceAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private String id;
    private SharedPreferences pref;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_announce, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((HomeActivity) getActivity()).closeDrawer();
        pref = getContext().getSharedPreferences("login.conf", Context.MODE_PRIVATE);
        id = pref.getString("uid", "");
        myAnnouncePresenter = new MyAnnouncePresenter(this);
        rcMyAnnunce = (RecyclerView) getActivity().findViewById(R.id.rcMyAnnunce);
        load();

    }

    public void load() {
        myAnnouncePresenter.load(id);
    }

    public void loadDelManRequest(List<RequestDeliveryModel> requestDeliveryModel) {

        if (!requestDeliveryModel.isEmpty()) {

            layoutManager = new LinearLayoutManager(getContext());
            rcMyAnnunce.setLayoutManager(layoutManager);
            myAnnounceAdapter = new MyAnnounceAdapter(getContext(), requestDeliveryModel, MyAnnounceFragment.this);
            rcMyAnnunce.setAdapter(myAnnounceAdapter);

        } else {
            Toast.makeText(getContext(), "Você não possui nenhum anúncio cadastrada!", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.openDrawer)
    public void onViewClicked() {

        ((HomeActivity) getActivity()).openDrawer();

    }

    public void deteleMyAnnounce(String id) {

        myAnnouncePresenter.deleteAnnounce(id);
    }

    public void deleteOK() {

        myAnnounceAdapter.notifyDataSetChanged();
        Toast.makeText(getContext(), "Anúncio deletado com sucesso!", Toast.LENGTH_LONG).show();

    }
}
