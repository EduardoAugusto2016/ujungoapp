package megaapps.com.br.ujungo.ActivityView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import megaapps.com.br.ujungo.Extendables.MyFragActivity;
import megaapps.com.br.ujungo.Presenter.SplashPresenter;
import megaapps.com.br.ujungo.R;

public class SplashActivity extends MyFragActivity {

    private static int SPLASH_TIME_OUT = 3000;

    SharedPreferences pref;

    private SplashPresenter splashPresenter ;

    private String email;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        splashPresenter = new SplashPresenter(this);

        pref = getSharedPreferences("login.conf", Context.MODE_PRIVATE);
        email = pref.getString("email", "");
        password = pref.getString("password", "");

        if (email.equals("") || password.equals("")) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();

                }
            }, SPLASH_TIME_OUT);

        }else{

            splashPresenter.getDataLogin(email, password);
        }
    }

    public void loginOk() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();

            }
        }, SPLASH_TIME_OUT);

    }

    public void loginError() {
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }
}
