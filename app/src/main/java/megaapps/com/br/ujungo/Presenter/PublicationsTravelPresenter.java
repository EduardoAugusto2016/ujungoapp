package megaapps.com.br.ujungo.Presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import megaapps.com.br.ujungo.Model.PublicationsTravelModel;
import megaapps.com.br.ujungo.Service.APIService;
import megaapps.com.br.ujungo.FragmentView.PublicationsTravelFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by duh on 7/15/17.
 */

public class PublicationsTravelPresenter {

    //private static String BASE_URL = "http://10.0.3.2/";
    private static String BASE_URL = "http://easytvteste2017.esy.es/";
    private List<PublicationsTravelModel> publicationsTravelModel;
    private PublicationsTravelFragment publicationsTravelFragment;
    private List<PublicationsTravelModel> newRequestDeliveryList = new ArrayList<>();

    public PublicationsTravelPresenter(PublicationsTravelFragment publicationsTravelFragment) {
        this.publicationsTravelFragment = publicationsTravelFragment;
    }

    public void getDataDelManRequest() {

        publicationsTravelFragment.execLoading();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<List<PublicationsTravelModel>> call = service.loadWantdelivery();
        call.enqueue(new Callback<List<PublicationsTravelModel>>() {
            @Override
            public void onResponse(Call<List<PublicationsTravelModel>> call, Response<List<PublicationsTravelModel>> response) {

                if (response.isSuccessful()) {

                    publicationsTravelModel = response.body();

                    if (publicationsTravelModel != null) {

                        for (int i = 0; i < publicationsTravelModel.size(); i++) {

                            if(publicationsTravelModel.get(i).getAccept().equals("false")){

                                newRequestDeliveryList.add(publicationsTravelModel.get(i));

                            }
                        }

                        publicationsTravelFragment.loadDelManRequest(newRequestDeliveryList);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<PublicationsTravelModel>> call, Throwable t) {
                publicationsTravelFragment.stopLoading();
            }
        });
    }
}
