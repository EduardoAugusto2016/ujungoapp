package megaapps.com.br.ujungo.FragmentView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import megaapps.com.br.ujungo.ActivityView.HomeActivity;
import megaapps.com.br.ujungo.Extendables.MyFragment;
import megaapps.com.br.ujungo.R;

/**
 * Created by mariliavilas on 04/07/17.
 */

public class ItemAcceptAddFragment extends MyFragment {

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_accept_add, container, false);
        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((HomeActivity) getActivity()).closeDrawer();
    }
}
