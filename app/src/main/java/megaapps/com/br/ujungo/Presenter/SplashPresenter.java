package megaapps.com.br.ujungo.Presenter;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import megaapps.com.br.ujungo.Model.UserModel;
import megaapps.com.br.ujungo.Service.APIService;
import megaapps.com.br.ujungo.ActivityView.SplashActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by duh on 6/21/17.
 */

public class SplashPresenter {

    //private static String BASE_URL = "http://10.0.3.2/";
    private static String BASE_URL = "http://easytvteste2017.esy.es/";

    private UserModel userModel;

    private SplashActivity splashActivity;

    public SplashPresenter(SplashActivity splashActivity) {
        this.splashActivity = splashActivity;
    }


    public void getDataLogin(String email, String password) {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<UserModel> call = service.login(email, password);
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {

                if (response.isSuccessful()) {

                    userModel = response.body();

                    if (userModel.getResultCode().equals("OK")) {

                        splashActivity.loginOk();

                    } else if (userModel.getResultCode().equals("NO")) {

                        splashActivity.loginError();

                    }
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {

                Log.d("splasherror", t.getMessage());

            }
        });


    }
}
