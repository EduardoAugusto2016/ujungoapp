package megaapps.com.br.ujungo.ActivityView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;
import megaapps.com.br.ujungo.Extendables.MyFragActivity;
import megaapps.com.br.ujungo.FragmentView.AccountFragment;
import megaapps.com.br.ujungo.FragmentView.AnnounceInProgressFragment;
import megaapps.com.br.ujungo.FragmentView.HomeFragment;
import megaapps.com.br.ujungo.FragmentView.MyAnnounceFragment;
import megaapps.com.br.ujungo.FragmentView.MyTravelFragment;
import megaapps.com.br.ujungo.R;
import megaapps.com.br.ujungo.Util.ActivityResultBus;
import megaapps.com.br.ujungo.Util.ActivityResultEvent;
import megaapps.com.br.ujungo.Util.CustomFontTextView;

public class HomeActivity extends MyFragActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    private String nameUser;
    private String emailUser;
    private String avatarUser;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        pref = getSharedPreferences("login.conf", Context.MODE_PRIVATE);
        nameUser = pref.getString("name", "");
        emailUser = pref.getString("email", "");
        avatarUser = pref.getString("avatar", "");

        HomeFragment homeFragment = new HomeFragment();

        getMyFragTransaction().add(R.id.frContent, homeFragment);
        getMyFragTransaction().commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);

        CustomFontTextView name = (CustomFontTextView) header.findViewById(R.id.nameUser);
        CustomFontTextView email = (CustomFontTextView) header.findViewById(R.id.emailUser);
        CircleImageView avatar = (CircleImageView) header.findViewById(R.id.avatarUser);

        if (!avatarUser.isEmpty()) {

            Glide.with(this)
                    .load(avatarUser)
                    .thumbnail(0.5f)
                    .into(avatar);
        } else {

            Glide.with(this)
                    .load(R.drawable.ic_account)
                    .thumbnail(0.5f)
                    .into(avatar);

        }

        name.setText(nameUser);
        email.setText(emailUser);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ActivityResultBus.getInstance().postQueue(
                new ActivityResultEvent(requestCode, resultCode, data));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

            HomeActivity homeactivity = (this);
            HomeFragment frag = new HomeFragment();
            FragmentTransaction ft = homeactivity.getSupportFragmentManager().beginTransaction();
            ft.addToBackStack("home");
            ft.replace(R.id.frContent, frag);
            ft.commit();

        } else if (id == R.id.nav_account) {

            HomeActivity homeactivity = (this);
            AccountFragment frag = new AccountFragment();
            FragmentTransaction ft = homeactivity.getSupportFragmentManager().beginTransaction();
            ft.addToBackStack("account");
            ft.replace(R.id.frContent, frag);
            ft.commit();

        } else if (id == R.id.nav_request_delivery) {

            HomeActivity homeactivity = (this);
            AnnounceInProgressFragment frag = new AnnounceInProgressFragment();
            FragmentTransaction ft = homeactivity.getSupportFragmentManager().beginTransaction();
            ft.addToBackStack("AnnounceInProgressFragment");
            ft.replace(R.id.frContent, frag);
            ft.commit();

        } else if (id == R.id.nav_my_delivery) {

            HomeActivity homeactivity = (this);
            MyAnnounceFragment frag = new MyAnnounceFragment();
            FragmentTransaction ft = homeactivity.getSupportFragmentManager().beginTransaction();
            ft.addToBackStack("MyAnnounceFragment");
            ft.replace(R.id.frContent, frag);
            ft.commit();

        } else if (id == R.id.nav_my_travel) {

            HomeActivity homeactivity = (this);
            MyTravelFragment frag = new MyTravelFragment();
            FragmentTransaction ft = homeactivity.getSupportFragmentManager().beginTransaction();
            ft.addToBackStack("MyTravelFragment");
            ft.replace(R.id.frContent, frag);
            ft.commit();

        } else if (id == R.id.nav_exit) {

            pref = getSharedPreferences("login.conf", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.clear();
            editor.commit();
            startActivity(new Intent(HomeActivity.this, LoginActivity.class));
            finish();
            return true;

        } else if (id == R.id.nav_contact) {

        } else if (id == R.id.nav_about) {

        }

        openDrawer();
        return true;
    }


    public void openDrawer() {
        drawer.openDrawer(GravityCompat.START);
    }

    public void closeDrawer() {
        drawer.closeDrawers();
    }

}
