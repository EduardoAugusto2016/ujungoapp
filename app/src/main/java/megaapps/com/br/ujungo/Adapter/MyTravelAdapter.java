package megaapps.com.br.ujungo.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import megaapps.com.br.ujungo.FragmentView.MyTravelFragment;
import megaapps.com.br.ujungo.Model.WantDeliveryManModel;
import megaapps.com.br.ujungo.R;

/**
 * Created by mariliavilas on 09/09/17.
 */

public class MyTravelAdapter extends RecyclerView.Adapter<MyTravelAdapter.ReqDelManPubRecyclerHolder> {

    private Context context;
    private List<WantDeliveryManModel> requestTravelList;
    private MyTravelFragment myTravelFragment;

    public MyTravelAdapter(Context context, List<WantDeliveryManModel> requestTravelList, MyTravelFragment myTravelFragment) {
        this.context = context;
        this.requestTravelList = requestTravelList;
        this.myTravelFragment = myTravelFragment;
    }

    @Override
    public ReqDelManPubRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_my_travel, parent, false);
        return new ReqDelManPubRecyclerHolder(view);
    }

    @Override
    public void onBindViewHolder(final ReqDelManPubRecyclerHolder holder, final int position) {

        ReqDelManPubRecyclerHolder viewHolder = (ReqDelManPubRecyclerHolder) holder;

        viewHolder.txtTittle.setText(requestTravelList.get(position).getTittle());
        viewHolder.txtCountry.setText(requestTravelList.get(position).getCountry());
        viewHolder.txtCity.setText(requestTravelList.get(position).getCity());

        if (requestTravelList.get(position).getTravelType().equals("Internacional")) {

            Glide.with(context).load(R.drawable.ic_international).into(viewHolder.imgAnnounce);

        } else if (requestTravelList.get(position).getTravelType().equals("Nacional")) {

            Glide.with(context).load(R.drawable.ic_nacional).into(viewHolder.imgAnnounce);
        }

        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myTravelFragment.deteleMyTravel(requestTravelList.get(position).getId());

            }
        });

    }

    @Override
    public int getItemCount() {
        return requestTravelList.size();
    }

    public class ReqDelManPubRecyclerHolder extends RecyclerView.ViewHolder {

        private ImageButton btnDelete;
        private ImageView imgAnnounce;
        private TextView txtAnnunce, txtCountry, txtCity, txtTittle;

        ReqDelManPubRecyclerHolder(View itemView) {
            super(itemView);

            btnDelete = (ImageButton) itemView.findViewById(R.id.btnDelete);
            txtAnnunce = (TextView) itemView.findViewById(R.id.txtAnnunce);
            txtCountry = (TextView) itemView.findViewById(R.id.txtCountry);
            txtCity = (TextView) itemView.findViewById(R.id.txtCity);
            txtTittle = (TextView) itemView.findViewById(R.id.txtTittle);
            imgAnnounce = (ImageView) itemView.findViewById(R.id.imgAnnounce);


        }
    }
}
