package megaapps.com.br.ujungo.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import megaapps.com.br.ujungo.Model.PublicationsTravelModel;
import megaapps.com.br.ujungo.R;
import megaapps.com.br.ujungo.ActivityView.HomeActivity;
import megaapps.com.br.ujungo.FragmentView.ItemAcceptAddFragment;

/**
 * Created by duh on 7/15/17.
 */

public class PublicationTravelAdapter extends RecyclerView.Adapter<PublicationTravelAdapter.ReqDelManPubRecyclerHolder> {

    Context context;
    private List<PublicationsTravelModel> requestTravelList;
    private HomeActivity homeactivity;

    private String id;

    public PublicationTravelAdapter(Context context, List<PublicationsTravelModel> requestDeliveryModel) {
        this.context = context;
        this.requestTravelList = requestDeliveryModel;
    }

    @Override
    public ReqDelManPubRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_publications_travel, parent, false);
        return new ReqDelManPubRecyclerHolder(view);
    }

    @Override
    public void onBindViewHolder(final ReqDelManPubRecyclerHolder holder, final int position) {

        ReqDelManPubRecyclerHolder viewHolder = (ReqDelManPubRecyclerHolder) holder;

        viewHolder.txtTittle.setText(requestTravelList.get(position).getTittle());
        viewHolder.txtAddress.setText("Destino: " +
                requestTravelList.get(position).getCountry() + " - " +
                requestTravelList.get(position).getCity()
        );

        viewHolder.txtDate.setText("Data: " + requestTravelList.get(position).getDate());

        if (requestTravelList.get(position).getTraveltype().equals("Internacional")) {

            Glide.with(context).load(R.drawable.ic_international).into(viewHolder.imgAnnounce);

        } else if (requestTravelList.get(position).getTraveltype().equals("Nacional")) {

            Glide.with(context).load(R.drawable.ic_nacional).into(viewHolder.imgAnnounce);
        }
        if (requestTravelList.get(position).getBagsize().equals("pequeno")) {

            Glide.with(context).load(R.drawable.ic_bag_one_gray).into(viewHolder.imgSize);

        }
        if (requestTravelList.get(position).getBagsize().equals("médio")) {

            Glide.with(context).load(R.drawable.ic_bag_two_gray).into(viewHolder.imgSize);

        }
        if (requestTravelList.get(position).getBagsize().equals("grande")) {

            Glide.with(context).load(R.drawable.ic_bag_tree_gray).into(viewHolder.imgSize);

        }
        if (requestTravelList.get(position).getBagsize().equals("enorme")) {

            Glide.with(context).load(R.drawable.ic_bag_four_gray).into(viewHolder.imgSize);
        }

        ((ReqDelManPubRecyclerHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                requestTravelList.get(position).getId();
                homeactivity = (HomeActivity) context;
                ItemAcceptAddFragment frag = new ItemAcceptAddFragment();
                FragmentTransaction ft = homeactivity.getSupportFragmentManager().beginTransaction();
                ft.addToBackStack("acceptAdd");
                ft.replace(R.id.frContent, frag);
                ft.commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return requestTravelList.size();
    }

    public class ReqDelManPubRecyclerHolder extends RecyclerView.ViewHolder {

        public TextView txtTittle, txtDate, txtAddress;
        public ImageView imgAnnounce, imgSize;

        ReqDelManPubRecyclerHolder(View itemView) {
            super(itemView);

            imgAnnounce = (ImageView) itemView.findViewById(R.id.imgAnnounce);
            txtTittle = (TextView) itemView.findViewById(R.id.txtTittle);
            txtAddress = (TextView) itemView.findViewById(R.id.txtAddress);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            imgSize = (ImageView) itemView.findViewById(R.id.imgSize);

        }
    }
}
