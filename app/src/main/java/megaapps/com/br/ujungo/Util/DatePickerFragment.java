package megaapps.com.br.ujungo.Util;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.Calendar;

import megaapps.com.br.ujungo.R;

/**
 * Created by duh on 29/09/16.
 */

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        final Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), this, year, month, day);
        dpDialog.getDatePicker().setMinDate(c.getTimeInMillis());

        return dpDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {

        TextView editDate = (TextView) getActivity().findViewById(R.id.editDate);
        editDate.setText(day + "/" + month + "/" + year);

    }
}

