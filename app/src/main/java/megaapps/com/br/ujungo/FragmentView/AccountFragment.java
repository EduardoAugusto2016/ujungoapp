package megaapps.com.br.ujungo.FragmentView;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.otto.Subscribe;
import java.io.File;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import megaapps.com.br.ujungo.ActivityView.HomeActivity;
import megaapps.com.br.ujungo.Extendables.MyFragment;
import megaapps.com.br.ujungo.Model.UserModel;
import megaapps.com.br.ujungo.Presenter.AccountPresenter;
import megaapps.com.br.ujungo.R;
import megaapps.com.br.ujungo.Service.APIService;
import megaapps.com.br.ujungo.Util.ActivityResultBus;
import megaapps.com.br.ujungo.Util.ActivityResultEvent;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by duh on 6/24/17.
 */

public class AccountFragment extends MyFragment {

    private static String BASE_URL = "http://10.0.3.2/";
    private UserModel userModel;
    @BindView(R.id.openDrawer)
    Button openDrawer;
    @BindView(R.id.txtAccount)
    TextView txtAccount;
    @BindView(R.id.input_layout_passactual)
    TextInputLayout inputLayoutPassactual;
    @BindView(R.id.input_layout_newpass)
    TextInputLayout inputLayoutNewpass;
    @BindView(R.id.input_layout_newpass_c)
    TextInputLayout inputLayoutNewpassC;
    @BindView(R.id.laySearch)
    LinearLayout laySearch;
    @BindView(R.id.profile_image)
    CircleImageView profile_image;
    @BindView(R.id.txtEditAvatar)
    TextView txtEditAvatar;
    @BindView(R.id.TittleLoading)
    TextView TittleLoading;
    @BindView(R.id.load)
    RelativeLayout load;
    @BindView(R.id.txtNavTittle)
    TextView txtNavTittle;
    @BindView(R.id.laytop)
    RelativeLayout laytop;
    @BindView(R.id.layimg)
    RelativeLayout layimg;
    @BindView(R.id.layLoading)
    RelativeLayout layLoading;
    @BindView(R.id.scrowData)
    ScrollView scrowData;
    @BindView(R.id.editName)
    EditText editName;
    @BindView(R.id.editPhone)
    EditText editPhone;
    @BindView(R.id.editEmail)
    EditText editEmail;
    @BindView(R.id.editPassActual)
    EditText editPassActual;
    @BindView(R.id.editNewPass)
    EditText editNewPass;
    @BindView(R.id.editNewPassC)
    EditText editNewPassC;
    @BindView(R.id.txtPassTittle)
    TextView txtPassTittle;
    @BindView(R.id.txtAddAddress)
    TextView txtAddAddress;
    @BindView(R.id.btnSave)
    Button btnSave;
    Unbinder unbinder;

    private String mediaPath = "";
    private AccountPresenter accountPresenter;
    private String id;
    private String pass;
    public static final int CODE_GALLERY = 1;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((HomeActivity) getActivity()).closeDrawer();

        accountPresenter = new AccountPresenter(this);
        pref = getContext().getSharedPreferences("login.conf", Context.MODE_PRIVATE);
        id = pref.getString("uid", "");
        pass = pref.getString("password", "");
        accountPresenter.LoadData(id);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.txtEditAvatar, R.id.txtAddAddress, R.id.btnSave, R.id.openDrawer})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtEditAvatar:
                openCam();
                break;
            case R.id.txtAddAddress:

                HomeActivity homeactivity = (HomeActivity) getActivity();
                AddAddressFragment addAddressFragment = new AddAddressFragment();
                FragmentTransaction ftDeliveryMan = homeactivity.getSupportFragmentManager().beginTransaction();
                ftDeliveryMan.addToBackStack("add_address");
                ftDeliveryMan.replace(R.id.frContent, addAddressFragment);
                ftDeliveryMan.commit();

                break;
            case R.id.btnSave:

                if (
                        mediaPath.isEmpty() ||
                                editName.getText().toString().isEmpty() ||
                                editEmail.getText().toString().isEmpty() ||
                                editPhone.getText().toString().isEmpty() ||
                                editPassActual.getText().toString().isEmpty() ||
                                editNewPass.getText().toString().isEmpty() ||
                                editNewPassC.getText().toString().isEmpty()
                        ) {

                    if (mediaPath.isEmpty()) {
                        emptyAvatar();
                        return;
                    }


                    if (editName.getText().toString().isEmpty()) {
                        emptyName();
                        return;
                    }
                    if (editEmail.getText().toString().isEmpty()) {
                        emptyEmail();
                        return;
                    }
                    if (editPhone.getText().toString().isEmpty()) {
                        emptyPhone();
                        return;
                    }
                    if (editPassActual.getText().toString().isEmpty()) {
                        emptyPassActual();
                        return;
                    }
                    if (editNewPass.getText().toString().isEmpty()) {
                        emptyNewPass();
                        return;
                    }
                    if (editNewPassC.getText().toString().isEmpty()) {
                        emptyNewPassC();
                        return;
                    }


                } else if (!editNewPass.getText().toString().equals(editNewPassC.getText().toString())) {
                    passNotEquals();
                    return;

                } else if (editNewPass.getText().toString().length() < 6 || editNewPassC.getText().toString().length() < 6) {
                    passMinorSix();
                    return;

                } else if (!editPassActual.getText().toString().equals(pass)) {
                    passActualError();
                    return;

                } else {

                    execLoading();
                    uploadFile();

                }
                break;

            case R.id.openDrawer:
                ((HomeActivity) getActivity()).openDrawer();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        ActivityResultBus.getInstance().register(mActivityResultSubscriber);
    }

    @Override
    public void onStop() {
        super.onStop();
        ActivityResultBus.getInstance().unregister(mActivityResultSubscriber);
    }

    @Override
    public void onResume() {
        super.onResume();
        ActivityResultBus.getInstance();
    }

    private Object mActivityResultSubscriber = new Object() {
        @Subscribe
        public void onActivityResultReceived(ActivityResultEvent event) {
            int requestCode = event.getRequestCode();
            int resultCode = event.getResultCode();
            Intent data = event.getData();
            onActivityResult(requestCode, resultCode, data);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (data == null) {

                Toast.makeText(getContext(), R.string.noImageSelected, Toast.LENGTH_SHORT).show();

            } else {

                if (requestCode == CODE_GALLERY) {

                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContext().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    assert cursor != null;
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    mediaPath = cursor.getString(columnIndex);

                    Glide.with(getContext())
                            .load(mediaPath)
                            .into(profile_image);

                    cursor.close();

                }
            }

        } catch (Exception e) {
            Toast.makeText(getContext(), R.string.noImageSelected, Toast.LENGTH_LONG).show();
        }

    }

    private void openCam() {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2);

        } else if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);

        } else {
            Intent i = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            getActivity().startActivityForResult(i, CODE_GALLERY);
        }

    }

    private void uploadFile() {

        File file = new File(mediaPath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), requestBody);

        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), editName.getText().toString());
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), editEmail.getText().toString());
        RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), editPhone.getText().toString());
        RequestBody newpass = RequestBody.create(MediaType.parse("text/plain"), editNewPass.getText().toString());


        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<UserModel> call = service.updateAccount(
                fileToUpload,
                id,
                name,
                email,
                phone,
                newpass);
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {

                if (response.isSuccessful()) {
                    userModel = response.body();

                    if (userModel.getResultCode().equals("OK")) {

                        stopLoading();
                        updateOK();

                    }
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {

                stopLoading();
            }
        });
    }

    public void emptyName() {
        editName.setError(getText(R.string.emptyName));
        editName.requestFocus();
    }

    public void emptyEmail() {
        editEmail.setError(getText(R.string.emptyEmail));
        editEmail.requestFocus();
    }

    public void emptyPhone() {
        editName.setError(getText(R.string.emptyPhone));
        editName.requestFocus();
    }

    public void emptyPassActual() {
        editPassActual.setError(getText(R.string.emptyPassActual));
        editPassActual.requestFocus();
    }

    public void emptyNewPass() {
        editNewPass.setError(getText(R.string.emptyNewPass));
        editNewPass.requestFocus();
    }

    public void emptyNewPassC() {
        editNewPassC.setError(getText(R.string.emptyNewPassC));
        editNewPassC.requestFocus();
    }

    public void loadData(String name, String email, String phone, String avatar) {

        editName.setText(name);
        editEmail.setText(email);
        editPhone.setText(phone);

        if (!avatar.isEmpty()) {

            Glide.with(getContext())
                    .load(avatar)
                    .thumbnail(0.5f)
                    .into(profile_image);
        }
    }

    public void execLoading() {

        layLoading.setVisibility(View.VISIBLE);
        scrowData.setVisibility(View.GONE);

    }

    public void stopLoading() {

        layLoading.setVisibility(View.GONE);
        scrowData.setVisibility(View.VISIBLE);

    }

    public void passNotEquals() {

        editNewPassC.setError(getText(R.string.passNotEquals));
        editNewPass.setError(getText(R.string.passNotEquals));
        editNewPass.requestFocus();

    }

    public void updateOK() {

        editPassActual.setText("");
        editNewPass.setText("");
        editNewPassC.setText("");

        Toast.makeText(getContext(), R.string.updateOK, Toast.LENGTH_LONG).show();

    }

    public void passMinorSix() {

        editNewPassC.setError(getText(R.string.passMinorSix));
        editNewPass.setError(getText(R.string.passMinorSix));
        editNewPass.requestFocus();

    }

    public void passActualError() {

        editPassActual.setError(getText(R.string.passActualError));
        editPassActual.requestFocus();

    }

    private void emptyAvatar() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(AccountFragment.this.getString(R.string.dialogAtention));
        builder.setIcon(R.drawable.ic_warning);
        builder.setMessage(this.getString(R.string.noavatar));
        builder.show();

    }

}






