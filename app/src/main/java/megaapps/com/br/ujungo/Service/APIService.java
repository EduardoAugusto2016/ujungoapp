package megaapps.com.br.ujungo.Service;


import java.util.List;

import megaapps.com.br.ujungo.Model.AddAddressModel;
import megaapps.com.br.ujungo.Model.PublicationsTravelModel;
import megaapps.com.br.ujungo.Model.RequestDeliveryModel;
import megaapps.com.br.ujungo.Model.UserModel;
import megaapps.com.br.ujungo.Model.WantDeliveryManModel;
import retrofit2.Call;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Multipart;
import retrofit2.http.Part;


/**
 * Created by duh on 6/20/17.
 */

public interface APIService {

    @FormUrlEncoded
    @POST("/ws_ujungo/UserData/login.php")
    Call<UserModel> login(
            @Field("email") String email,
            @Field("pass") String pass
    );

    @FormUrlEncoded
    @POST("/ws_ujungo/UserData/register.php")
    Call<UserModel> register(
            @Field("name") String name,
            @Field("email") String email,
            @Field("pass") String pass
    );

    @FormUrlEncoded
    @POST("/ws_ujungo/Read/account_data.php")
    Call<UserModel> load(
            @Field("id") String id
    );

    @Multipart
    @POST("/ws_ujungo/Update/update_profile.php")
    Call<UserModel> updateAccount(
            @Part MultipartBody.Part file,
            @Part("id") String id,
            @Part("name") RequestBody name,
            @Part("email") RequestBody email,
            @Part("phone") RequestBody phone,
            @Part("newpass") RequestBody newPass
    );

    @Multipart
    @POST("/ws_ujungo/Create/request_delivery.php")
    Call<RequestDeliveryModel> createRequestDelivery(
            @Part MultipartBody.Part file,
            @Part("id") String id,
            @Part("size") RequestBody size,
            @Part("tittle") RequestBody tittle,
            @Part("info") RequestBody info,
            @Part("addressPub") RequestBody addressPub,
            @Part("numberPub") RequestBody numberPub,
            @Part("neighborhoodPub") RequestBody neighborhoodPub,
            @Part("cityPub") RequestBody cityPub,
            @Part("statePub") RequestBody statePub,
            @Part("cepPub") RequestBody cepPub,
            @Part("AddresseePub") RequestBody AddresseePub,
            @Part("datePub") RequestBody datePub,
            @Part("valuePub") RequestBody valuePub
    );

    @FormUrlEncoded
    @POST("/ws_ujungo/Create/want_delivery.php")
    Call<WantDeliveryManModel> createWantDeliveryMan(
            @Field("id") String id,
            @Field("bagsize") String bagSize,
            @Field("tittle") String tittle,
            @Field("info") String info,
            @Field("data") String data,
            @Field("traveltype") String travelType,
            @Field("city") String city,
            @Field("country") String country);

    @FormUrlEncoded
    @POST("/ws_ujungo/Update/update_address.php")
    Call<AddAddressModel> updateAddress(
            @Field("id") String id,
            @Field("address") String address,
            @Field("number") String number,
            @Field("neighborhood") String neighborhood,
            @Field("zipcode") String zipcode,
            @Field("city") String city,
            @Field("state") String state,
            @Field("country") String country);

    @FormUrlEncoded
    @POST("/ws_ujungo/Read/load_address.php")
    Call<AddAddressModel> loadAddress(
            @Field("id") String id
    );

    @GET("/ws_ujungo/Read/load_deliveryman.php")
    Call<List<RequestDeliveryModel>> loadDelManRequest();


    @GET("/ws_ujungo/Read/load_wantdelivery.php")
    Call<List<PublicationsTravelModel>> loadWantdelivery();

    @FormUrlEncoded
    @POST("/ws_ujungo/Read/load_my_deliveryman.php")
    Call<List<RequestDeliveryModel>> loadMyDelManRequest(
            @Field("id") String id
    );

    @FormUrlEncoded
    @POST("/ws_ujungo/Read/load_my_wantdelivery.php")
    Call<List<WantDeliveryManModel>> loadMyTravel(
            @Field("id") String id
    );

    @FormUrlEncoded
    @POST("/ws_ujungo/Delete/delete_travel.php")
    Call<PublicationsTravelModel> deleteMyTravel(
            @Field("id") String id
    );

    @FormUrlEncoded
    @POST("/ws_ujungo/Delete/delete_announce.php")
    Call<RequestDeliveryModel> deleteMyAnnounce(
            @Field("id") String id
    );
}
