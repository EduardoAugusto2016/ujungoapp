package megaapps.com.br.ujungo.ActivityView;

import android.os.Bundle;
import butterknife.BindView;
import butterknife.ButterKnife;
import megaapps.com.br.ujungo.Extendables.MyFragActivity;
import megaapps.com.br.ujungo.R;
import megaapps.com.br.ujungo.Util.CustomFontButton;
import megaapps.com.br.ujungo.Util.CustomFontEditText;

public class ForgotActivity extends MyFragActivity {

    @BindView(R.id.editEmail)
    CustomFontEditText editEmail;
    @BindView(R.id.btnLogin)
    CustomFontButton btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        ButterKnife.bind(this);
    }
}
