package megaapps.com.br.ujungo.ActivityView;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import megaapps.com.br.ujungo.Extendables.MyFragActivity;
import megaapps.com.br.ujungo.Presenter.RegisterPresenter;
import megaapps.com.br.ujungo.R;
import megaapps.com.br.ujungo.Util.CustomFontButton;
import megaapps.com.br.ujungo.Util.CustomFontEditText;
import megaapps.com.br.ujungo.Util.CustomFontTextView;

public class RegisterActivity extends MyFragActivity {

    @BindView(R.id.layLoading)
    RelativeLayout layLoading;
    @BindView(R.id.scrollDataReg)
    ScrollView scrollDataReg;
    @BindView(R.id.tittleLoading)
    CustomFontTextView tittleLoading;
    @BindView(R.id.editName)
    CustomFontEditText editName;
    @BindView(R.id.editEmail)
    CustomFontEditText editEmail;
    @BindView(R.id.editPass)
    CustomFontEditText editPass;
    @BindView(R.id.editPassConf)
    CustomFontEditText editPassConf;
    @BindView(R.id.btnReg)
    CustomFontButton btnReg;


    private RegisterPresenter registerPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        registerPresenter = new RegisterPresenter(this);
    }

    @OnClick(R.id.btnReg)
    public void onViewClicked() {

        registerPresenter.getDataRegister(
                editName.getText().toString(),
                editEmail.getText().toString(),
                editPass.getText().toString(),
                editPassConf.getText().toString());

    }

    public void nameEmpty() {

        editName.requestFocus();
        editName.setError("Preencha o campo nome!");

    }

    public void emailEmpty() {

        editEmail.requestFocus();
        editEmail.setError("Preencha o campo email!");

    }

    public void passwordEmpty() {

        editPass.requestFocus();
        editPass.setError("Preencha o campo senha!");

    }

    public void passwordCEmpty() {

        editPassConf.requestFocus();
        editPassConf.setError("Preencha o campo confirmar senha!");

    }

    public void passwordNotEquals() {

        editPass.requestFocus();
        editPass.setError("Preencha o campo senha!");
        editPassConf.setError("Preencha o campo confirmar senha!");

    }

    public void registerOk() {

        finish();
        Toast.makeText(RegisterActivity.this, "Cadastro realizado com sucesso!", Toast.LENGTH_LONG).show();
    }

    public void emailExists() {
        editEmail.requestFocus();
        editEmail.setError("Este email já está sendo utilizado!");
    }

    public void execLoading() {
        layLoading.setVisibility(View.VISIBLE);
        scrollDataReg.setVisibility(View.GONE);
    }

    public void stopLoading() {

        layLoading.setVisibility(View.GONE);
        scrollDataReg.setVisibility(View.VISIBLE);

    }
}
